import sys

from pandas._libs import algos

from storage import ConvergenceStorage
from convergence import ConvergenceValues
import pandas as pd
import math


def ranking(dim):
    cs = ConvergenceStorage()

    prefixes = ["a", "b", "c", "d", "e"]

    means_df = pd.DataFrame({"Fun": [], "DISH": [], "CLPSO": [], "DBLSHADE": []})
    for prefix in prefixes:
        total_range = 16
        if prefix == "e":
            total_range = 11
        for fun in range(1, total_range):
            dish = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "DISH"))
            clpso = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "CLPSO"))
            dblshade = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "DblSHADE"))

            row = [f"{prefix.capitalize()}{fun}", dish.mean[len(dish.mean)-1], clpso.mean[len(clpso.mean)-1], dblshade.mean[len(dblshade.mean)-1]]
            means_df.loc[len(means_df.index)] = row

    print(means_df.to_latex(index=False, float_format=lambda x: "{:.3E}".format(x)))
    # print(means_df.to_latex(index=False))
    # print(means_df.to_latex())

    # print(means_df.iloc[0, 2:])
    # print(means_df.iloc[0, 2:].to_latex(float_format=lambda x: "{:.2f}".format(x)))
    # print(means_df.iloc[1, 2].tostring(float_format="%.2f"))

    # print(means_df)

    ranks_df = means_df.iloc[:, 1:].rank(axis="columns", ascending=True)
    # ranks_df["Fun"] = means_df["Fun"]
    ranks_df.insert(0, "Fun", means_df["Fun"])

    sums = ranks_df.iloc[:, 1:].sum()
    ranks_df.loc[len(ranks_df.index)] = ["SUM", sums[0], sums[1], sums[2]]
    print(ranks_df.to_latex(index=False, float_format=lambda x: "{:.1f}".format(x)))
    # print(ranks_df)

def get_best():
    cs = ConvergenceStorage()
    prefixes = ["a", "b", "c", "d", "e"]
    algos = ["DISH", "CLPSO", "DblSHADE"]
    dimensions = [10, 30]
    dim = 30

    for prefix in prefixes:
        for fun in range(1, 16):
            if prefix == "e" and fun > 10:
                return
            global_best = ["none", float("inf"), []]
            disq = []
            for algo in algos:
                convergence = ConvergenceValues(cs.load_convergence(prefix, fun, dim, algo))
                algo_bests = cs.load_bests(prefix, fun, dim, algo)
                for i in range(len(convergence.values)):
                    if any(math.isnan(x) for x in algo_bests[0]):
                        disq.append(algo)
                        continue
                    if convergence.values[i][len(convergence.values[i])-1] < global_best[1]:
                        lb_value = convergence.values[i][len(convergence.values[i])-1]
                        lb_vec = algo_bests[i]
                        global_best = [algo, lb_value, lb_vec]

            print(f"\multicolumn{{11}}{{l}}{{\\textbf{{{prefix.capitalize()}{fun}}}}} \\\\")
            for i in range(0, len(global_best[2]), 10):
                res = (f"$x_{{{i+1}}}$ - $x_{{{i+10}}}$ & {global_best[2][i]:.2E} & {global_best[2][i+1]:.2E} & "
                       # res = (f"\\textbf{{{prefix.capitalize()}{fun}}} & {global_best[2][i]:.2E} & {global_best[2][i+1]:.2E} & "
                       f"{global_best[2][i+2]:.2E} & {global_best[2][i+3]:.2E} & {global_best[2][i+4]:.2E} & "
                       f"{global_best[2][i + 5]:.2E} & {global_best[2][i + 6]:.2E} & {global_best[2][i + 7]:.2E} & "
                       f"{global_best[2][i + 8]:.2E} & {global_best[2][i + 9]:.2E} \\\\")
                print(res)
            print("\\midrule")
            print(f"\multicolumn{{2}}{{l}}{{Optimum ({global_best[0]})}} & \multicolumn{{9}}{{r}}{{{global_best[1]:.10E}}} \\\\")
            print("\\midrule")
            print("\\midrule")
            # print()

    # dish = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "DISH"))
    # clpso = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "CLPSO"))
    # dblshade = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "DblSHADE"))


if __name__ == "__main__":
    ranking(30)
    # get_best()




    pass



