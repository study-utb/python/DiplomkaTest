import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import TestFunctions.bm_sets as bms


class ChartPrinter:
    def __init__(self, steps):
        if not os.path.exists(f"Images"):
            os.makedirs(f"Images")
        self.__steps = steps
        lin_x = np.linspace(-100, 100, steps)
        lin_y = np.linspace(-100, 100, steps)
        self.__x, self.__y = np.meshgrid(lin_x, lin_y)
        self.__sets = bms.BmSets()

    def print(self, fun):
        z = np.zeros([self.__steps, self.__steps])
        for x in range(self.__steps):
            for y in range(self.__steps):
                z[x, y] = self.__sets.count(fun, pd.Series([self.__x[x, y], self.__y[x, y]]))
        name = f"{self.__sets.get_fun_class(fun)}_{self.__sets.get_fun_name(fun)}"
        self.__print_chart3d(z, name, None)

    def __print_chart3d(self, values, name, title):
        plt.rcParams['figure.figsize'] = [10, 10]
        ax = plt.axes(projection='3d')
        ax.plot_surface(self.__x, self.__y, values, cmap='Accent', alpha=0.8)
        if title is not None:
            ax.set_title(title + ' (3D)')
        ax.set_xlabel(r"$x_1$", fontsize=16)
        ax.set_ylabel(r"$x_2$", fontsize=16)
        ax.set_zlabel(r"$f(x_1, x_2)$", fontsize=16)
        plt.savefig(f"Images/Functions/{name}_3d.png")
        # plt.show()
        plt.close()
        return



def main():
    cp = ChartPrinter(1000)
    # Rastrigin
    # cp.print(10)
    cp.print(24)
    for i in range(20):
        cp.print(i)


if __name__ == "__main__":
    main()
