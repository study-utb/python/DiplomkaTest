import os
from storage import ConvergenceStorage
import numpy as np
import matplotlib.pyplot as plt


class ConvergenceValues:
    def __init__(self, array):
        self.values = array
        self.min = []
        self.max = []
        self.mean = []
        self.__analyze()

    def __analyze(self):
        for col in range(self.values.shape[1]):
            row_values = []
            for row in range(self.values.shape[0]):
                row_values.append(self.values[row][col])
            self.min.append(min(row_values))
            self.max.append(max(row_values))
            self.mean.append(np.mean(row_values))


class ConvergenceChartPrinter:
    def __init__(self):
        self.__path = f"{os.path.dirname(os.path.abspath(__file__))}/Data/Convergence/Charts"
        if not os.path.exists(self.__path):
            os.makedirs(self.__path)

    def print_convergence_chart(self, values: list, labels: list = None, show=True, save=False, name=""):
        plt.rcParams['figure.figsize'] = [15, 10]
        use_labels = labels is not None and len(values) == len(labels)

        for i in range(len(values)):
            if use_labels:
                plt.plot(values[i], label=labels[i])
            else:
                plt.plot(values[i])

        plt.xlabel("Počet iterací")
        plt.ylabel("f(x)")
        if use_labels:
            plt.legend()
        if save:
            plt.savefig(f"{self.__path}/{name}.png")
        if show:
            plt.show()

        plt.close()


def get_convergence_charts(dim):
    prefixes = ["a", "b", "c", "d"]
    cs = ConvergenceStorage()
    cp = ConvergenceChartPrinter()
    for fun in range(1, 16):
        for prefix in prefixes:
            dish = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "DISH"))
            clpso = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "CLPSO"))
            dblshade = ConvergenceValues(cs.load_convergence(prefix, fun, dim, "DblSHADE"))

            cp.print_convergence_chart(dish.values, show=False, save=True, name=f"f({prefix})-{fun:06d}-{dim}D-DISH")
            cp.print_convergence_chart(clpso.values, show=False, save=True, name=f"f({prefix})-{fun:06d}-{dim}D-CLPSO")
            cp.print_convergence_chart(dblshade.values, show=False, save=True, name=f"f({prefix})-{fun:06d}-{dim}D-DblSHADE")
            cp.print_convergence_chart([dish.mean, clpso.mean, dblshade.mean], ["DISH", "CLPSO", "DblSHADE"],
                                       show=False, save=True, name=f"f({prefix})-{fun:06d}-{dim}D-XCOMPARE")


if __name__ == "__main__":
    # get_convergence_charts(10)
    # get_convergence_charts(30)

    cs = ConvergenceStorage()
    con = cs.load_convergence_ap("e", [])
    print(con)
    cp = ConvergenceChartPrinter()
    cp.print_convergence_chart(con, show=True, save=True, name="AP(e)")


