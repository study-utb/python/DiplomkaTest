import TestFunctions.bm_sets as bms
import TestFunctions.gen_sets as gs
from pflacco.sampling import create_initial_sample
import pflacco.classical_ela_features as cf
from sklearn.preprocessing import StandardScaler
import time
import os
import print_time
import matplotlib.pyplot as plt
import pandas as pd
import ela


class DataGatherer:
    def __init__(self, dim=10, repeats=30):
        self.__mask = ela.features
        self.__dim = dim
        self.__repeats = repeats
        self.__lower_bound = -100
        self.__upper_bound = 100
        self.__calculations = ela.calculations
        self.data_table = pd.DataFrame()
        self.__data_path = f"{os.path.dirname(os.path.abspath(__file__))}/Data"
        # vytvoření potřebných adresářů, pokud neexistují
        if not os.path.exists(self.__data_path):
            os.makedirs(self.__data_path)

        try:
            self.data_table = pd.read_csv(f"{self.__data_path}/ela_basic_{self.__dim}D.csv", index_col=0)
        except:
            self.recount_data()

        self.scaler = StandardScaler()
        self.original_data_scaled = self.scaler.fit_transform(self.data_table.drop(columns=["Class", "Name"], axis=1))

    def recount_data(self):
        sets = bms.BmSets()
        print(f"Provádím výpočty ELA na {sets.functions_count} funkcích pro {self.__dim}D")
        start_time_total = time.time()
        columns_inited = False
        for iteration in range(self.__repeats):
            start_time = time.time()
            X = create_initial_sample(self.__dim, sample_type='lhs', lower_bound=self.__lower_bound, upper_bound=self.__upper_bound)

            for fun_index in range(sets.functions_count):
                result_dictionary = {"Class": sets.get_fun_class(fun_index), "Name": sets.get_fun_name(fun_index)}

                for calculation in self.__calculations:
                    y = X.apply(lambda x: sets.count(fun_index, x), axis=1)
                    raw_result = calculation(X, y)
                    result = {}
                    for key in raw_result.keys():
                        if key in self.__mask:
                            result[key] = raw_result[key]
                    result_dictionary = {**result_dictionary, **result}

                if not columns_inited:
                    self.data_table = pd.DataFrame(columns=list(result_dictionary.keys()))
                    columns_inited = True

                self.data_table.loc[len(self.data_table.index)] = result_dictionary.values()

            end_time = time.time()
            print("")
            print(f"{iteration}. iterace dokončena za : {print_time.float_to_time(end_time - start_time)}")

        end_time_total = time.time()
        print("")
        print("+--------------------------+")
        print(f"| total time: {print_time.float_to_time(end_time_total - start_time_total)} |")
        print("+--------------------------+")

        del sets
        self.data_table.to_csv(f"{self.__data_path}/ela_basic_{self.__dim}D.csv")

    def get_normalized_data_table(self):
        normalized_table = self.data_table.copy()
        for col in normalized_table.columns[2:]:
            min = normalized_table[col].min()
            max = normalized_table[col].max()
            if max - min > 0:
                normalized_table[col] = normalized_table[col].apply(lambda x: (x - min) / (max - min))
            else:
                normalized_table[col] = 0
        return normalized_table


class GenDataGatherer:
    def __init__(self, prefix, recount=False, dim=10, repeats=30):
        self.__mask = ela.features
        self.prefix = prefix
        self.__dim = dim
        self.__repeats = repeats
        self.__lower_bound = -100
        self.__upper_bound = 100
        self.__calculations = ela.calculations
        self.data_table = pd.DataFrame()
        self.__data_path = f"{os.path.dirname(os.path.abspath(__file__))}/Data"
        # vytvoření potřebných adresářů, pokud neexistují
        if not os.path.exists(self.__data_path):
            os.makedirs(self.__data_path)

        if not recount:
            try:
                self.data_table = pd.read_csv(f"{self.__data_path}/ela_{self.prefix}_{self.__dim}D.csv", index_col=0)
            except:
                self.recount_data()
        else:
            self.recount_data()

        self.scaler = StandardScaler()
        self.original_data_scaled = self.scaler.fit_transform(self.data_table.drop(columns=["Class", "Name"], axis=1))

    def recount_data(self):
        sets = gs.GenSets(self.prefix)
        print(f"Provádím výpočty ELA na {len(sets.formulas)} generovaných funkcích pro {self.__dim}D s prefixem {self.prefix}")
        start_time_total = time.time()
        columns_inited = False
        for form_index in range(len(sets.formulas)):
            start_time = time.time()
            X = create_initial_sample(self.__dim, sample_type='lhs', lower_bound=self.__lower_bound, upper_bound=self.__upper_bound)

            for iteration in range(self.__repeats):

                result_dictionary = {"Class": "Generated", "Name": f"{self.prefix} {sets.formula_names[form_index]}"}

                err_found = False
                for calculation in self.__calculations:
                    y = X.apply(lambda x: sets.formulas[form_index].evaluate(x), axis=1)

                    raw_result = calculation(X, y)

                    for val in raw_result.values():
                        if val == float("inf"):
                            print(f"Nalezena hodnota inf v ELA features funkce {self.prefix} {sets.formula_names[form_index]}, funkce je vynechána")
                            err_found = True
                            break

                    if err_found:
                        break

                    result = {}
                    for key in raw_result.keys():
                        if key in self.__mask:
                            result[key] = raw_result[key]
                    result_dictionary = {**result_dictionary, **result}

                if err_found:
                    break

                if not columns_inited:
                    self.data_table = pd.DataFrame(columns=list(result_dictionary.keys()))
                    columns_inited = True

                self.data_table.loc[len(self.data_table.index)] = result_dictionary.values()

            end_time = time.time()
            print(f"Funkce {sets.prefix} {sets.formula_names[form_index]} dokončena za : {print_time.float_to_time(end_time - start_time)}")

        end_time_total = time.time()
        print("")
        print("+--------------------------+")
        print(f"| total time: {print_time.float_to_time(end_time_total - start_time_total)} |")
        print("+--------------------------+")

        del sets
        self.data_table.to_csv(f"{self.__data_path}/ela_{self.prefix}_{self.__dim}D.csv")



def main():
    # dg = DataGatherer()
    # gdg = GenDataGatherer("a", recount=True)
    # gdg = GenDataGatherer("b", recount=True)
    # gdg = GenDataGatherer("c", recount=True)
    # gdg = GenDataGatherer("d", recount=True)
    # gdg = GenDataGatherer("e", recount=True)
    gdg = GenDataGatherer("z", recount=True)

if __name__ == "__main__":
    main()
