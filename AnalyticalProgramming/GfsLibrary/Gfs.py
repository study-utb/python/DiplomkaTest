import AnalyticalProgramming.GfsLibrary.Gfs0 as Gfs0
import AnalyticalProgramming.GfsLibrary.Gfs1 as Gfs1
import AnalyticalProgramming.GfsLibrary.Gfs2 as Gfs2

#        0               1               2               3               4              5              6              7
gfs_0 = [Gfs0.GfParamX1, Gfs0.GfParamX2, Gfs0.GfParamSS, Gfs0.GfConstPi, Gfs0.GfConstE, Gfs0.GfConstI, Gfs0.GfConstD, Gfs0.GfReinforcement]
gfs_levels = 0

#                8           9           10           11          12           13          14
gfs_1 = gfs_0 + [Gfs1.GfInv, Gfs1.GfAbs, Gfs1.GfPow2, Gfs1.GfSin, Gfs1.GfSin2, Gfs1.GfCos, Gfs1.GfSqrtAbs]
gfs_levels += 1

#                15          16          17          18
gfs_2 = gfs_1 + [Gfs2.GfAdd, Gfs2.GfSub, Gfs2.GfMul, Gfs2.GfDiv]
gfs_levels += 1

gfs_all = gfs_2





