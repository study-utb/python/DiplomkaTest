import sys

import AnalyticalProgramming.GfsLibrary.GfsAbstractDefinitions as GfsDef
import math


class GfParamX1(GfsDef.Gfs0):
    def __init__(self):
        pass

    def count(self, args, rs):
        return args[0]

    def str(self):
        return "x[i]"

    def formula(self):
        return "x[i]"

    def latex(self, rs):
        return " x_i "


class GfParamX2(GfsDef.Gfs0):
    def __init__(self):
        pass

    def count(self, args, rs):
        return args[1]

    def str(self):
        return "x[i+1]"

    def formula(self):
        return "x[i+1]"

    def latex(self, rs):
        return " x_{i+1} "


class GfParamSS(GfsDef.Gfs0):
    def __init__(self):
        pass

    def count(self, args, rs):
        return math.sqrt(math.pow(args[0], 2) + math.pow(args[1], 2))

    def str(self):
        return "sqrt(x[i]^2 + (x[i+1]^2)"

    def formula(self):
        return "math.sqrt(math.pow(x[i], 2) + math.pow(x[i+1], 2))"

    def latex(self, rs):
        return " \\sqrt{x_i^2 + x_{i+1}^2} "


class GfConstPi(GfsDef.Gfs0):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        return math.pi

    def str(self):
        return "pi"

    def formula(self):
        return "math.pi"

    def latex(self, rs):
        return " \\pi "


class GfConstE(GfsDef.Gfs0):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        return math.e

    def str(self):
        return "e"

    def formula(self):
        return "math.e"

    def latex(self, rs):
        return " e "


class GfConstI(GfsDef.Gfs0):
    def __init__(self, index, value):
        self.index = index
        self.value = int(math.ceil(value * 100))

    def count(self, args, rs):
        return self.value

    def str(self):
        return f"{self.value}"

    def formula(self):
        return f"{self.value}"

    def latex(self, rs):
        return f" {str(self.value)} "


class GfConstD(GfsDef.Gfs0):
    def __init__(self, index, value):
        self.index = index
        if value == 0:
            self.value = sys.float_info.epsilon
        else:
            self.value = value

    def count(self, args, rs):
        return self.value

    def str(self):
        return f"{self.value}"

    def formula(self):
        return f"{self.value}"

    def latex(self, rs):
        return f" {str(self.value)} "


class GfReinforcement(GfsDef.Gfs0):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        if rs is not None:
            rs.evaluate(args)

        return args[0]

    def str(self):
        return "omega"

    def formula(self):
        return "omega"

    def latex(self, rs):
        if rs:
            return " \\omega "
        else:
            return " x_i "
