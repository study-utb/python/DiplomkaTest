import AnalyticalProgramming.GfsLibrary.GfsAbstractDefinitions as GfsDef
import math
import sys


class GfInv(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        if self.n_count == 0:
            self.n_count = sys.float_info.epsilon

        try:
            res = 1 / self.n_count
        except Exception:
            raise Exception(f"Chyba převrácené hodnoty GfInv({self.n_count})")

        return res

    def str(self):
        return f"(1/{self.node[0].str()})"

    def formula(self):
        return f"(1/{self.node[0].formula()})"

    def latex(self, rs):
        return f" \\left( \\frac{{1}} {{{self.node[0].latex(rs)}}}\\right) "


class GfAbs(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = abs(self.n_count)
        except Exception:
            raise Exception(f"Chyba absolutní hodnoty GfAbs({self.n_count})")

        return res

    def str(self):
        return f"|{self.node[0].str()}|"

    def formula(self):
        return f"abs({self.node[0].formula()})"

    def latex(self, rs):
        return f" \\left|{self.node[0].latex(rs)}\\right| "


class GfPow2(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = math.pow(self.n_count, 2)
        except Exception:
            raise Exception(f"Chyba mocniny GfPow2({self.n_count}, 2)")

        return res

    def str(self):
        return f"({self.node[0].str()})^2"

    def formula(self):
        return f"math.pow({self.node[0].formula()}, 2)"

    def latex(self, rs):
        return f" \\left({self.node[0].latex(rs)}\\right)^2 "


class GfSin(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = math.sin(self.n_count)
        except Exception:
            raise Exception(f"Chyba sin GfSin({self.n_count})")

        return res

    def str(self):
        return f"sin({self.node[0].str()})"

    def formula(self):
        return f"math.sin({self.node[0].formula()})"

    def latex(self, rs):
        return f" sin\\left({self.node[0].latex(rs)}\\right) "


class GfSin2(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = math.pow(math.sin(self.n_count), 2)
        except Exception:
            raise Exception(f"Chyba sin^2 GfSin2({self.n_count})")

        return res

    def str(self):
        return f"sin^2({self.node[0].str()})"

    def formula(self):
        return f"math.pow(math.sin({self.node[0].formula()}), 2)"

    def latex(self, rs):
        return f" sin^2\\left({self.node[0].latex(rs)}\\right) "


class GfCos(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = math.cos(self.n_count)
        except Exception:
            raise Exception(f"Chyba cos GfCos({self.n_count})")

        return res

    def str(self):
        return f"cos({self.node[0].str()})"

    def formula(self):
        return f"math.cos({self.node[0].formula()})"

    def latex(self, rs):
        return f" cos\\left({self.node[0].latex(rs)}\\right) "


class GfSqrtAbs(GfsDef.Gfs1):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = math.sqrt(abs(self.n_count))
        except Exception as e:
            raise Exception(f"Výjimka odmocniny GfSqrtAbs({self.n_count}), {e}")

        return res

    def str(self):
        return f"sqrt(|{self.node[0].str()}|)"

    def formula(self):
        return f"math.sqrt(abs({self.node[0].formula()}))"

    def latex(self, rs):
        return f" \\sqrt{{\\left|{self.node[0].latex(rs)}\\right|}} "


# class GfExp(GfsDef.Gfs1):
#     def __init__(self):
#         super().__init__()
#
#     def count(self, args):
#         super().count(args)
#
#         try:
#             res = math.exp(self.n_count)
#         except Exception as e:
#             raise Exception(f"Chyba argumentu GfExp({self.n_count}), {e}")
#
#         return res
#
#     def str(self):
#         return f"exp({self.node[0].str()})"
#
#     def formula(self):
#         return f"math.exp({self.node[0].formula()})"
#
#     def latex(self):
#         return f"{{e}}^{{{self.node[0].latex()}}}"





