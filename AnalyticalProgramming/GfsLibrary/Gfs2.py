import sys

import AnalyticalProgramming.GfsLibrary.GfsAbstractDefinitions as GfsDef
import math


class GfAdd(GfsDef.Gfs2):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = self.lr_count[0] + self.lr_count[1]
        except Exception:
            raise Exception(f"Chyba sčítání GfAdd({self.lr_count[0]}, {self.lr_count[1]})")

        return res

    def str(self):
        return f"({self.node_l[0].str()} + {self.node_r[0].str()})"

    def formula(self):
        return f"({self.node_l[0].formula()} + {self.node_r[0].formula()})"

    def latex(self, rs):
        return f" \\left({self.node_l[0].latex(rs)}+{self.node_r[0].latex(rs)}\\right) "


class GfSub(GfsDef.Gfs2):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = self.lr_count[0] - self.lr_count[1]
        except Exception:
            raise Exception(f"Chyba odčítání GfSub({self.lr_count[0]}, {self.lr_count[1]})")

        return res

    def str(self):
        return f"({self.node_l[0].str()} - {self.node_r[0].str()})"

    def formula(self):
        return f"({self.node_l[0].formula()} - {self.node_r[0].formula()})"

    def latex(self, rs):
        return f" \\left({self.node_l[0].latex(rs)}-{self.node_r[0].latex(rs)}\\right) "


class GfMul(GfsDef.Gfs2):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)

        try:
            res = self.lr_count[0] * self.lr_count[1]
        except Exception:
            raise Exception(f"Chyba násobení GfMul({self.lr_count[0]}, {self.lr_count[1]})")

        return res

    def str(self):
        return f"({self.node_l[0].str()} * {self.node_r[0].str()})"

    def formula(self):
        return f"({self.node_l[0].formula()} * {self.node_r[0].formula()})"

    def latex(self, rs):
        return f" \\left({self.node_l[0].latex(rs)}\\cdot{self.node_r[0].latex(rs)}\\right) "


class GfDiv(GfsDef.Gfs2):
    def __init__(self):
        super().__init__()

    def count(self, args, rs):
        super().count(args, rs)
        if self.lr_count[1] == 0:
            #raise Exception("Výjimka dělení nulou")
            self.lr_count[1] = sys.float_info.epsilon

        try:
            res = self.lr_count[0] / self.lr_count[1]
        except Exception:
            raise Exception(f"Chyba dělení GfDiv({self.lr_count[0]}, {self.lr_count[1]})")

        return res

    def str(self):
        return f"(({self.node_l[0].str()}) / ({self.node_r[0].str()}))"

    def formula(self):
        return f"(({self.node_l[0].formula()}) / ({self.node_r[0].formula()}))"

    def latex(self, rs):
        return f" \\frac{{{self.node_l[0].latex(rs)}}} {{{self.node_r[0].latex(rs)}}} "


