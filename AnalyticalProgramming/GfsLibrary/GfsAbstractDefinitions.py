from abc import ABC, abstractmethod


class MyFunc(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def count(self, args, rs):
        pass

    @abstractmethod
    def str(self):
        pass

    @abstractmethod
    def formula(self):
        pass

    @abstractmethod
    def latex(self, rs):
        pass


class Gfs2(MyFunc, ABC):
    def __init__(self):
        self.node_l = []
        self.node_r = []
        self.lr_count = []

    def count(self, args, rs):
        try:
            l_value = self.node_l[0].count(args, rs)
        except Exception as e:
            raise e
        try:
            r_value = self.node_r[0].count(args, rs)
        except Exception as e:
            raise e

        self.lr_count = [l_value, r_value]


class Gfs1(MyFunc, ABC):
    def __init__(self):
        self.node = []
        self.n_count = 0

    def count(self, args, rs):
        try:
            value = self.node[0].count(args, rs)
        except Exception as e:
            raise e

        self.n_count = value


class Gfs0(MyFunc, ABC):
    @abstractmethod
    def __init__(self):
        pass
