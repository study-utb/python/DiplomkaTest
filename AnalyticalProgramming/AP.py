import math
import numbers
import numpy as np
import AnalyticalProgramming.GfsLibrary.Gfs as Gfs
import AnalyticalProgramming.DiscreteSetHandling as Dsh
import Strategy.AP_DISH as DISH
import data_gatherer as dg
import ela
from centroids import load_clusters
from storage import FormulaStorage
from loger import log
from Strategy.AP_DISH import Individual

class AnalyticalProgramming:
    """
    Třída Analytického Programování

    Argumenty konstruktoru:
        function_legth (int)
            Jak dlouhý bude vektor, kde se tvoří funkce (bez prostoru pro konstanty, ten se dopočítá sám).
        ap_population_size (int)
            Velikost populace pro DE, který bude vyvíjet výslednou funkci.
        ev_population_size (int)
            Velikost populace pro DE a PSO, kde se bude vyvíjená funkce testovat.

    Metody třídy:
        restart()
            Vytvoří novou instanci DE algoritmu pro AP a vymaže nejlepší vygenerovanou funkci
        next_generation()
            Provede evoluci AP pomocí algoritmu DE o 1 generaci
        next_generations(int)
            Provede evoluci AP pomocí algoritmu DE o více generací
    """

    def __init__(self, experiment):
        """
        :param function_length: Jak dlouhý bude vektor, kde se tvoří funkce (bez prostoru pro konstanty, ten se
                                dopočítá sám)
        :param population_size: Velikost populace pro DE, který bude vyvíjet výslednou funkci
        :param dim=10: kolik dimenzí budou vytvářené funkce zpracovávat (výchozí hodnota 10, aby bylo može srovnávat s CEC)
        """
        self.__gfs_borders = (0, len(Gfs.gfs_all)-1)  # (include, exclude)
        self.__constants_borders = (0, 1)
        self.__function_length = experiment[2]  # délka vektoru pro funkce
        self.__arguments_length = self.__function_length - math.floor(
            (self.__function_length - 1) / Gfs.gfs_levels)  # délka vektoru pro konstanty

        self.strategy = None
        self.__maxFEs = experiment[3]

        self.__data_gatherer = dg.DataGatherer()
        self.__original_data_scaled = self.__data_gatherer.original_data_scaled

        self.__clusters = load_clusters(experiment[1])

        self.__best_vector_without_omega = None      # vektor pro posílené hledání (nesmí používat posílené hledání)
        self.__value_of_vector_without_omega = None  # hodnota vektoru pro sosílené hledání
        self.best_solution = None                    # nejlepší nalezené řešení

    def start(self):
        self.strategy = DISH.DISH(self.__function_length, [0, len(Gfs.gfs_all) - 1],
                                    self.__arguments_length, self.__constants_borders,
                                    self.__maxFEs, self.objective_function, 5, 5)

        best, history = self.strategy.run()
        return best, history

    def objective_function(self, formula_as_vector):
        formula = Dsh.create_formula(formula_as_vector)
        if formula.dimension == 0:
            return None, False, False

        try:
            ela_df = ela.analyze(formula, "AP generated", "try", 1)
        except Exception as e:
            return None, False, True
        ela_features = ela_df.drop(columns=["Class", "Name"], axis=1).values

        # v objective function budu generovat jen jeden vektor ela features, takže smyčka row by proběhla jen 1x
        # for row in range(len(ela_features)):
        for col in range(len(ela_features[0])):
            if not isinstance(ela_features[0, col], numbers.Number):
                return None, False, True
            if ela_features[0, col] == float("inf"):
                return None, False, True

        try:
            scaled = self.__data_gatherer.scaler.transform(ela_df.drop(columns=["Class", "Name"], axis=1))
        except Exception as e:
            log(f"\nVýjimka scaleru: {e}\n{e.with_traceback}\n"
                f"Vyvoláno vektorem: {formula_as_vector}\n"
                f"ELA features: {ela_df.drop(columns=['Class', 'Name'], axis=1)}", True)
            return None, False, True
        # kosinová podobnost, nebo euklidovská vználenost, nebo euklidovská vzdálenost bez odmocniny
        dist = self.__get_nearest_cluster1(scaled)

        return -dist, formula.uses_reinforcement, True

    def __get_nearest_cluster1(self, vector):
        """Zjistí nejbližší cluster pomocí euklidovské vzdálenosti bez odmocniny"""
        distance = np.sum((vector - self.__clusters[0])**2)
        for i in range(1, len(self.__clusters)):
            d = np.sum((vector - self.__clusters[i])**2)
            if d < distance:
                distance = d

        return distance


def test():
    # "kmeans_clusters", "ela_centroids"
    ap = AnalyticalProgramming(["a", "kmeans_clusters", 20, 500])
    best, history = ap.start()
    print(best)
    # fs = FormulaStorage()
    # fs.save_formula(best, "xxx")





if __name__ == "__main__":
    test()
