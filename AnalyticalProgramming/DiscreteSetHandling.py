import math
import AnalyticalProgramming.Formula as Formula
import AnalyticalProgramming.GfsLibrary.Gfs as Gfs
import AnalyticalProgramming.GfsLibrary.GfsAbstractDefinitions as GfsDef
import AnalyticalProgramming.GfsLibrary.Gfs0 as Gfs0


# Délka programu l=11
# Max úroveň GFS = 2 (gfs_2)
# Rozšíření k = 11 - math.floor((11-1)/2) = 11 - math.floor(5) = 6
# Celková délka vektoru je l+k = 11 + 6 = 17
# Délka prodloužení z celkové délky vektoru - math.ceil(len(vec) / 3) = math.ceil(17/3) = 6

#             | prostor pro program                      || prostor pro konstanty |
#             | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
# test_list = [11,  7,  0, 12,  1,  5, 12,  1, 12,  2,  0,  10,  2,  0,  0,  0,  0]

def create_formula(vector):
    formula = __list_to_formula(vector[0])
    if formula.uses_reinforcement and len(vector[1]) > 0:
        reinf = __list_to_formula(vector[1])
        reinf.check_dimensions(True)
        if reinf.dimension > 0 and not reinf.uses_reinforcement:
            formula.rs_formula = reinf
        else:
            formula.rs_formula = None

    formula.check_dimensions(True)
    return formula

def __list_to_formula(vector):
    constant_len = math.ceil(len(vector) / 3)
    program_len = len(vector) - constant_len
    formula = Formula.Formula(None)
    constant_index = 0
    module_index = 0

    modules = []
    for i in range(0, program_len):
        modules.append(int(round(vector[i])))
    constants = vector[program_len:]
    # print(f"Modules: {modules}")
    # print(f"Constants: {constants}")

    # převede vektor čísel na objekt formula
    while not formula.is_complete():
        if formula.free_slots() < len(modules) - 1 - module_index:
            module = Gfs.gfs_all[modules[module_index]]
        elif formula.free_slots() == len(modules) - 1 - module_index:
            module = Gfs.gfs_1[modules[module_index] % len(Gfs.gfs_1)]
        else:
            module = Gfs.gfs_0[modules[module_index] % len(Gfs.gfs_0)]

        if issubclass(module, GfsDef.Gfs0):
            if issubclass(module, Gfs0.GfConstD) or issubclass(module, Gfs0.GfConstI):
                formula.add_item(module(constant_index, constants[constant_index % len(constants)]))
                constant_index += 1
            else:
                formula.add_item(module())
        else:
            formula.add_item(module())

        module_index += 1

    formula.check_dimensions(False)
    return formula


def test():
    print("Test Security Procedures")
    print("Test x1 + x1:")
    #             | prostor pro program                      || prostor pro konstanty |
    #             | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15,  1,  1, 12,  1,  5, 12,  1, 12,  2,  0,  10,  2,  0,  0,  0,  0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()
    print("Test x1 + x2:")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 0, 1, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()
    print("Test x2 + x2, musí se změnit na x1 + x1:")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 1, 1, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()
    print("Test neobsahuje x")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 3, 3, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()
    print("Test se součtem")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 2, 3, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()
    print("Test prázdného reinforcement x2 + r")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 1, 7, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()
    print("Test x1 + x2 + reinforcement:")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 15, 0, 1, 7, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0],
                 [15, 1, 1, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0]]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcement: {formula.uses_reinforcement}")
    print()
    print("Test x2 + x2 + reinforcement x2 + x2:")
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[15, 15, 1, 1, 7, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0],
                 [15, 1, 1, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0]]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcement: {formula.uses_reinforcement}")
    print()
    
    #           | prostor pro program                      || prostor pro konstanty |
    #           | 0|  1|  2|  3|  4|  5|  6|  7|  8|  9| 10|| 11| 12| 13| 14| 15| 16|
    test_list = [[1, 1, 7, 12, 1, 5, 12, 1, 12, 2, 0, 10, 2, 0, 0, 0, 0], []]
    formula = create_formula(test_list)
    print(f"> {formula.str()}")
    print(f"> Reinforcemen: {formula.uses_reinforcement}")
    print()


if __name__ == "__main__":
    test()
