import AnalyticalProgramming.GfsLibrary.GfsAbstractDefinitions as GfsDef
import AnalyticalProgramming.GfsLibrary.Gfs0 as Gfs0


class Formula:
    def __init__(self, rs_formula):
        self.__entry_point = []
        self.__free_slots = [self.__entry_point]
        self.__constants = []
        self.__evaluated = False
        self.dimension = 0
        self.uses_reinforcement = False
        self.rs_formula = rs_formula

    def evaluate(self, vector):
        if self.dimension == 0:
            raise Exception("neplatná funkce, neobsahuje žádné x")

        rs = None
        if self.uses_reinforcement:
            if isinstance(self.rs_formula, Formula):
                rs = self.rs_formula

        res = 0
        if self.dimension == 1:
            for i in range(len(vector)):
                try:
                    if rs is None:
                        res += self.__count([vector[i]], None)
                    else:
                        res += self.__count([vector[i]], rs)
                except Exception as e:
                    raise e
        else:
            for i in range(len(vector)-1):
                try:
                    if rs is None:
                        res += self.__count([vector[i], vector[i + 1]], None)
                    else:
                        res += self.__count([vector[i], vector[i+1]], rs)
                except Exception as e:
                    raise e

        return res

    def __count(self, args, rs):
        try:
            return self.__entry_point[0].count(args, rs)
        except Exception as e:
            raise e

    def str(self):
        if self.dimension == 0:
            return "neplatná funkce"

        res = self.__entry_point[0].str()
        if self.uses_reinforcement:
            if isinstance(self.rs_formula, Formula):
                res = f"{res}\nwhere omega is: {self.rs_formula.str()}"
            else:
                res = f"{res}\nwhere omega is: x[i]"
        return res

    def formula(self):
        if self.dimension == 0:
            return "neplatná funkce"

        res = self.__entry_point[0].formula()
        if self.uses_reinforcement:
            if isinstance(self.rs_formula, Formula):
                res = f"{res}\nwhere omega is: {self.rs_formula.formula()}"
            else:
                res = f"{res}\nwhere omega is: x[i]"
        return res

    def latex(self, reinforce=False):
        if self.dimension == 0:
            return "neplatná funkce"

        if reinforce:
            sum_string = "\\omega ="
        else:
            sum_string = "f(\\vec{x}) = \\sum_{i="
            if self.dimension == 1:
                sum_string += "1}^{D}"
            else:
                sum_string += "1}^{D-1}"

        use_rs = self.uses_reinforcement and isinstance(self.rs_formula, Formula)

        res = f"${sum_string} {self.__entry_point[0].latex(use_rs)}$"
        if use_rs:
            res = f"{res}\nwhere omega is: {self.rs_formula.latex(True)}"

        return res

    def is_complete(self):
        return len(self.__free_slots) == 0

    def add_item(self, item):
        free_slot = self.__free_slots.pop()
        free_slot.append(item)

        if isinstance(item, GfsDef.Gfs2):
            self.__free_slots.append(item.node_r)
            self.__free_slots.append(item.node_l)
        elif isinstance(item, GfsDef.Gfs1):
            self.__free_slots.append(item.node)
        else:
            pass

    def free_slots(self):
        return len(self.__free_slots)

    # Security Procedures

    def check_dimensions(self, reduce_dim):
        contains_x1 = False
        contains_x2 = False
        if isinstance(self.__entry_point[0], GfsDef.Gfs0):
            return

        slots = [self.__entry_point]
        while len(slots) > 0:
            item = slots.pop()[0]
            if isinstance(item, GfsDef.Gfs2):
                slots.append(item.node_r)
                slots.append(item.node_l)
            elif isinstance(item, GfsDef.Gfs1):
                slots.append(item.node)
            else:
                if isinstance(item, Gfs0.GfParamX1):
                    contains_x1 = True
                if isinstance(item, Gfs0.GfParamX2):
                    contains_x2 = True
                if isinstance(item, Gfs0.GfParamSS):
                    contains_x1 = True
                    contains_x2 = True
                if isinstance(item, Gfs0.GfReinforcement):
                    self.uses_reinforcement = True
                    contains_x1 = True
                    if self.rs_formula is not None:
                        if self.rs_formula.dimension == 2:
                            contains_x2 = True

        if contains_x1 and not contains_x2:
            self.dimension = 1
        elif contains_x2 and not contains_x1:
            if reduce_dim:
                self.__swap_x2_to_x1()
                self.dimension = 1
            else:
                self.dimension = 0
        elif contains_x1 and contains_x2:
            self.dimension = 2

    def __swap_x2_to_x1(self):
        slots = [self.__entry_point]
        while len(slots) > 0:
            item = slots.pop()[0]
            if isinstance(item, GfsDef.Gfs2):
                if isinstance(item.node_r[0], Gfs0.GfParamX2):
                    item.node_r[0] = Gfs0.GfParamX1()
                else:
                    slots.append(item.node_r)
                if isinstance(item.node_l[0], Gfs0.GfParamX2):
                    item.node_l[0] = Gfs0.GfParamX1()
                else:
                    slots.append(item.node_l)
            elif isinstance(item, GfsDef.Gfs1):
                if isinstance(item.node[0], Gfs0.GfParamX2):
                    item.node[0] = Gfs0.GfParamX1()
                else:
                    slots.append(item.node)





