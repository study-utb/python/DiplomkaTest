import AnalyticalProgramming.DiscreteSetHandling as Dsh
import dim_reduction as dr
import ela
import data_gatherer as dg
from storage import FormulaStorage
import pandas as pd
from centroids import load_clusters

# ofv: -184.25533692612484
# vector = [[16.70613603,  7.52277652, 13.03689871, 17.9631009, 3.14686621, 11.38976325,  7.15645009, 13.3797846, 6.33214402,  9.71939438, 16.55874517,  8.2786473, 13.62919227,  2.5896273,  5.07563819, 10.54361314, 14.91992208,  4.34738232, 10.61060962,  7.92190127, 0.35270892,  0.77862449,  0.30548302,  0.70699522,  0.38198621, 0.26741611,  0.81777339,  0.41089256,  0.65256108,  0.37298175, 0.49099871],
#           [15.50106712,  3.34705629, 13.21353725, 12.27791493, 2.45847937, 14.22411732, 16.34151486, 14.53990766, 3.23628941, 11.93186814, 17.20285879,  8.31738216, 17.78486678,  7.26520674,  5.17775159, 1.22237162, 11.44605186,  1.89006266, 13.11757323,  9.10606839, 0.35980225,  0.82940643,  0.18288785,  0.45122439,  0.09683591, 0.36505758,  0.6911952,  0.53258102,  0.72492233,  0.46607877, 0.69183298]]
#
# formula = Dsh.create_formula(vector)
# print(f"> {formula.str()}")
# print(f"> Reinforcemen: {formula.uses_reinforcement}")
# print()


def test():
    # fs = FormulaStorage()
    # formula = fs.load_formula("d", 1)
    # print(formula.str())
    # print(formula.dimension)
    #
    # # škálování a učení PCA na původních datech (DataGathereru)
    # data = ela.analyze(formula, "Generated", "x", 5)
    # data_gatherer = dg.DataGatherer()
    # try:
    #     data_scaled = data_gatherer.scaler.transform(data.drop(columns=["Class", "Name"], axis=1))
    # except Exception as e:
    #     print(f"chyba scaleru: {e}\n")
    #     for value in data.drop(columns=["Class", "Name"], axis=1).values:
    #         print(f"ELA: {value}")
    #     return
    #
    # pca = dr.PcaReduction(10, data_gatherer.data_table)
    # pca.highlight_data(data_scaled, True, False, "")

    # škálování a učení PCA na datech DataGathereru a sady "A", "B"
    sets_data = dg.DataGatherer().data_table.sort_values(["Class", "Name"])
    a_data = dg.GenDataGatherer("a").data_table
    b_data = dg.GenDataGatherer("b").data_table
    c_data = dg.GenDataGatherer("c").data_table
    d_data = dg.GenDataGatherer("d").data_table
    e_data = dg.GenDataGatherer("e").data_table
    z_data = dg.GenDataGatherer("z").data_table
    all_data = pd.concat([sets_data, a_data, b_data, c_data, d_data, e_data, z_data])
    # all_data = pd.concat([sets_data, a_data])
    # all_data = pd.concat([sets_data])
    all_data.reset_index(inplace=True, drop=True)

    # pca = dr.PcaReduction(10, all_data)

    # data = ela.analyze(formula, "Generated", "x", 5)
    # data_scaled = pca.scaler.transform(data.drop(columns=["Class", "Name"], axis=1))
    # pca.highlight_data(data_scaled, True, False, "")

    # pca.highlight_filtered_data_class("Generated", True, False, "")
    # pca.highlight_filtered_data_name_sw("Generated", "a", True, False, "")
    # pca.highlight_filtered_data_name_sw("Generated", "b", True, False, "")
    # pca.highlight_filtered_data_name_sw("Generated", "c", True, False, "")
    # pca.highlight_filtered_data_name_sw("Generated", "d", True, False, "")
    # pca.highlight_filtered_data_name_precise("Generated", "a 1", True, False, "")

    # škálování a učení TSNE na datech DataGathereru a sady "A", "B"
    # sets_data = dg.DataGatherer().data_table.sort_values(["Class", "Name"])
    # a_data = dg.GenDataGatherer("a").data_table
    # b_data = dg.GenDataGatherer("b").data_table
    # c_data = dg.GenDataGatherer("c").data_table
    # all_data = pd.concat([sets_data, a_data, b_data, c_data])
    # all_data.reset_index(inplace=True, drop=True)

    tsne = dr.TsneReduction(10, all_data)
    # tsne.highlight_filtered_data_class("Generated", True, False, "")
    # tsne.show_basic_data(True, True, "all2")
    # tsne.highlight_filtered_data_name_sw("Generated", "a", True, True, "a")
    # tsne.highlight_filtered_data_name_sw("Generated", "b", True, True, "b")
    # tsne.highlight_filtered_data_name_sw("Generated", "c", True, True, "c")
    # tsne.highlight_filtered_data_name_sw("Generated", "d", True, True, "d")
    # tsne.highlight_filtered_data_name_sw("Generated", "e", True, True, "e")
    # tsne.highlight_filtered_data_name_sw("Generated", "z", True, True, "z")
    # tsne.show_clusters_dbscan(True, False, "dbscan", 1.4, 15)
    # tsne.show_clusters_kmeans(True, False, "kmeans", 10)
    # tsne.show_clusters_optics(True, False, "optics", 0.75, 15)
    dish_and_db = ["a 5", "a 11", "c 9", "d 2", "d 10", "d 11", ]
    all_algs = ["a 4", "a 7", "c 1", "c 5", "e 3"]
    dish_filter = ["b 11", "d 14"]
    clpso_filter = ["a 1", "a 3", "b 1", "b 4", "b 6", "b 9", "b 10", "b 12", "c 13", "c 14", "d 5", "e 9"]
    dbl_shade_filter = ["a 2", "a 6", "a 8", "a 9", "a 10", "a 12", "a 13", "a 14", "a 15", "b 2", "b 3", "b 5", "b 7",
                        "b 8", "b 13", "b 14", "b 15", "c 2", "c 3", "c 4", "c 6", "c 7", "c 8", "c 10", "c 11", "c 12",
                        "c 15", "d 1", "d 3", "d 4", "d 6", "d 7", "d 8", "d 9", "d 12", "d 13", "d 15", "e 1", "e 2",
                        "e 4", "e 5", "e 6", "e 7", "e 8", "e 10"]
    tsne.highlight_filtered_data_name_precise("Generated", dish_filter + all_algs + dish_and_db, True, True, "dish_good")
    tsne.highlight_filtered_data_name_precise("Generated", clpso_filter + all_algs, True, True, "clpso_good")
    tsne.highlight_filtered_data_name_precise("Generated", dbl_shade_filter + all_algs + dish_and_db, True, True, "dbl_good")
    pass


if __name__ == "__main__":
    test()


