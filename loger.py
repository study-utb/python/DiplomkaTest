import time
import print_time as pt
import os


def log(message, save=False):
    message = f"{pt.float_to_time(time.time(), 2)}: {message}"
    print(message)
    if save:
        path = f"{os.path.dirname(os.path.abspath(__file__))}/log.txt"
        with open(path, "a") as file:
            file.write(message + "\n")

def test():
    log("test", True)


if __name__ == '__main__':
    test()
