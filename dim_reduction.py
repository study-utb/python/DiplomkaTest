import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import MeanShift
from sklearn.cluster import OPTICS
from yellowbrick.cluster import KElbowVisualizer
import numpy as np
import data_gatherer as dg
import pandas as pd
import os


class PcaReduction:
    def __init__(self, dim, data: pd.DataFrame):
        # vytvoření potřebných adresářů, pokud neexistují
        if not os.path.exists(f"Images"):
            os.makedirs(f"Images")
        self.__dim = dim
        original_data_raw = data.drop(columns=["Class", "Name"], axis=1)
        # self.original_classes = data["Class"]
        self.original_names = [data["Class"], data["Name"]]
        # vytvoření a natrénování StandardScaleru na stávajících datech
        self.scaler = StandardScaler()
        self.original_data_scaled = self.scaler.fit_transform(original_data_raw)

        self.__perplexity = 0

        self.tsne = None
        # vytvoření a natrénování PCA na stávajícíh datech
        self.pca = PCA(n_components=2)
        self.__original_data_transformed = self.pca.fit_transform(self.original_data_scaled)

    def highlight_filtered_data_class(self, class_filter, show, save, filename):
        data = []

        for i in range(len(self.original_names[0])):
            if self.original_names[0][i] == class_filter:
                data.append(self.__original_data_transformed[i])
        data = np.array(data)
        self.highlight_data(data, show, save, filename)

    def highlight_filtered_data_name_sw(self, class_filter, name_filter, show, save, filename):
        data = []

        for i in range(len(self.original_names[0])):
            if self.original_names[0][i] == class_filter and self.original_names[1][i].startswith(name_filter):
                data.append(self.__original_data_transformed[i])
        data = np.array(data)
        self.highlight_data(data, show, save, filename)

    def highlight_filtered_data_name_precise(self, class_filter, name_filter, show, save, filename):
        data = []

        for i in range(len(self.original_names[0])):
            if self.original_names[0][i] == class_filter and self.original_names[1][i] == name_filter:
                data.append(self.__original_data_transformed[i])
        data = np.array(data)
        self.highlight_data(data, show, save, filename)

    def show_basic_data(self, show, save, filename):
        plt.rcParams['figure.figsize'] = [15, 10]
        classes = self.original_names[0].unique()
        sorted_data = {}
        for c in classes:
            sorted_data[c] = []

        for i in range(len(self.original_names[0])):
            sorted_data[self.original_names[0][i]].append([self.__original_data_transformed[i, 0],
                                                         self.__original_data_transformed[i, 1]])

        for k in sorted_data.keys():
            new_x = []
            new_y = []
            for i in range(len(sorted_data[k])):
                new_x.append(sorted_data[k][i][0])
                new_y.append(sorted_data[k][i][1])

            plt.scatter(new_x, new_y, label=k)

        self.__print_chart(plt, show, save, filename, True)

    def highlight_data(self, data, show, save, filename):
        plt.rcParams['figure.figsize'] = [15, 10]
        plt.scatter(self.__original_data_transformed[:, 0], self.__original_data_transformed[:, 1], color="gray")
        plt.scatter(data[:, 0], data[:, 1], color="red")
        self.__print_chart(plt, show, save, filename, False)

    def show_clusters_dbscan(self, show, save, filename, eps, min_samples):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        dbscan_model = DBSCAN(eps=eps, min_samples=min_samples)
        dbscan_result = np.array(dbscan_model.fit_predict(self.original_data_scaled))
        # sort by cluster
        clusters = dbscan_result.max() + 2
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(dbscan_result)):
            sorted_data[dbscan_result[i]+1].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])
            if i == 0:
                plt.scatter(new_x, new_y, color="black", label="Noise")
            else:
                plt.scatter(new_x, new_y, label=i-1)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_kmeans(self, show, save, filename, clusters):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        kmeans_model = KMeans(n_clusters=clusters, n_init='auto')
        kmeans_result = np.array(kmeans_model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = kmeans_result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(kmeans_result)):
            sorted_data[kmeans_result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_ap(self, show, save, filename, damping):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        ap_model = AffinityPropagation(damping=damping)
        ap_result = np.array(ap_model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = ap_result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(ap_result)):
            sorted_data[ap_result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_meanshift(self, show, save, filename):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        model = MeanShift()
        result = np.array(model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(result)):
            sorted_data[result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_optics(self, show, save, filename, eps, min_samples):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        model = OPTICS(eps=eps, min_samples=min_samples)
        result = np.array(model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(result)):
            sorted_data[result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def __print_chart(self, plot, show, save, filename, show_legend):
        # plt.title(title)
        plt.grid(False)
        plot.xlabel("First Pricipal Component")
        plot.ylabel("Second Pricipal Component")
        if show_legend:
            plt.legend(loc="upper right")

        if save:
            if filename is not None:
                plot.savefig(f"Images/pca_{filename}_{self.__dim}D.png")
        if show:
            plot.show()
        plot.close()

    def vis(self):
        model = KMeans()
        visualizer = KElbowVisualizer(model, k=(1, 12))
        visualizer.fit(self.original_data_scaled)
        visualizer.show()


class TsneReduction:
    def __init__(self, dim, data, perplexity=30, steps=1000):
        # vytvoření potřebných adresářů, pokud neexistují
        if not os.path.exists(f"Images"):
            os.makedirs(f"Images")
        self.__dim = dim
        original_data_raw = data.drop(columns=["Class", "Name"], axis=1)
        # self.original_classes = data["Class"]
        self.original_names = [data["Class"], data["Name"]]
        # vytvoření a natrénování StandardScaleru na stávajících datech
        self.scaler = StandardScaler()
        self.original_data_scaled = self.scaler.fit_transform(original_data_raw)
        del data

        self.__perplexity = 0

        self.tsne = None
        # vytvoření a natrénování tSNE na stávajícíh datech
        self.tsne = TSNE(n_components=2, perplexity=perplexity, random_state=42, n_iter=steps)
        self.__original_data_transformed = self.tsne.fit_transform(self.original_data_scaled)


    def highlight_filtered_data_class(self, class_filter, show, save, filename):
        data = []

        for i in range(len(self.original_names[0])):
            if self.original_names[0][i] == class_filter:
                data.append(self.__original_data_transformed[i])
        data = np.array(data)
        self.highlight_data(data, show, save, filename)

    def highlight_filtered_data_name_sw(self, class_filter, name_filter, show, save, filename):
        data = []

        for i in range(len(self.original_names[0])):
            if self.original_names[0][i] == class_filter and self.original_names[1][i].startswith(name_filter):
                data.append(self.__original_data_transformed[i])
        data = np.array(data)
        self.highlight_data(data, show, save, filename)

    def highlight_filtered_data_name_precise(self, class_filter, name_filter, show, save, filename):
        data = []

        for i in range(len(self.original_names[0])):
            if self.original_names[0][i] == class_filter and self.original_names[1][i] in name_filter:
                data.append(self.__original_data_transformed[i])
        data = np.array(data)
        self.highlight_data(data, show, save, filename)



    def show_basic_data(self, show, save, filename):
        plt.rcParams['figure.figsize'] = [15, 10]
        classes = self.original_names[0].unique()
        sorted_data = {}
        for c in classes:
            sorted_data[c] = []

        for i in range(len(self.original_names[0])):
            sorted_data[self.original_names[0][i]].append([self.__original_data_transformed[i, 0],
                                                         self.__original_data_transformed[i, 1]])

        for k in sorted_data.keys():
            new_x = []
            new_y = []
            for i in range(len(sorted_data[k])):
                new_x.append(sorted_data[k][i][0])
                new_y.append(sorted_data[k][i][1])

            plt.scatter(new_x, new_y, label=k)

        self.__print_chart(plt, show, save, filename, True)

    def highlight_data(self, data, show, save, filename):
        plt.rcParams['figure.figsize'] = [15, 10]
        plt.scatter(self.__original_data_transformed[:, 0], self.__original_data_transformed[:, 1], color="gray")
        plt.scatter(data[:, 0], data[:, 1], color="red")
        self.__print_chart(plt, show, save, filename, False)


    def show_clusters_dbscan(self, show, save, filename, eps, min_samples):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        dbscan_model = DBSCAN(eps=eps, min_samples=min_samples)
        dbscan_result = np.array(dbscan_model.fit_predict(self.original_data_scaled))
        # sort by cluster
        clusters = dbscan_result.max() + 2
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(dbscan_result)):
            sorted_data[dbscan_result[i]+1].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])
            if i == 0:
                plt.scatter(new_x, new_y, color="black", label="Noise")
            else:
                plt.scatter(new_x, new_y, label=i-1)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_kmeans(self, show, save, filename, clusters):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        kmeans_model = KMeans(n_clusters=clusters, n_init='auto')
        kmeans_result = np.array(kmeans_model.fit_predict(self.original_data_scaled))
        # sort by cluster
        clusters = kmeans_result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(kmeans_result)):
            sorted_data[kmeans_result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_ap(self, show, save, filename, damping):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        ap_model = AffinityPropagation(damping=damping)
        ap_result = np.array(ap_model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = ap_result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(ap_result)):
            sorted_data[ap_result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_meanshift(self, show, save, filename):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        model = MeanShift()
        result = np.array(model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(result)):
            sorted_data[result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def show_clusters_optics(self, show, save, filename, eps, min_samples):
        plt.rcParams['figure.figsize'] = [15, 10]
        # self.original_data_scaled
        model = OPTICS(eps=eps, min_samples=min_samples)
        result = np.array(model.fit_predict(self.original_data_scaled))

        # sort by cluster
        clusters = result.max() + 1
        sorted_data = []
        for _ in range(clusters):
            sorted_data.append([])

        for i in range(len(result)):
            sorted_data[result[i]].append([self.__original_data_transformed[i, 0],
                                                    self.__original_data_transformed[i, 1]])

        for i in range(0, len(sorted_data)):
            new_x = []
            new_y = []
            for j in range(len(sorted_data[i])):
                new_x.append(sorted_data[i][j][0])
                new_y.append(sorted_data[i][j][1])

            plt.scatter(new_x, new_y, label=i)

        self.__print_chart(plt, show, save, filename, True)

    def __print_chart(self, plot, show, save, filename, show_legend):
        # plt.title(title)
        plt.grid(False)
        plot.xlabel("First t-SNE")
        plot.ylabel("Second t-SNE")
        if show_legend:
            plt.legend(loc="upper right")

        if save:
            if filename is not None:
                plot.savefig(f"Images/tsne_{filename}_{self.__dim}D.png")
        if show:
            plot.show()
        plot.close()


def test():
    sets_data = dg.DataGatherer().data_table.sort_values(["Class", "Name"])
    a_data = dg.GenDataGatherer("a").data_table
    all_data = pd.concat([sets_data, a_data])
    all_data.reset_index(inplace=True, drop=True)

    pca = PcaReduction(10, all_data)
    #
    # pca.highlight_filtered_data("Custom function", True, True, "custom")
    # pca.highlight_filtered_data("CEC2017", True, True, "cec2017")
    # pca.highlight_filtered_data("CEC2022", True, True, "cec2022")
    # pca.highlight_filtered_data_class("Generated", True, True, "generated")
    # pca.highlight_filtered_data_name_sw("Generated", "a", True, False, "generated_a")
    # pca.highlight_filtered_data_name_precise("Generated", "a 1", True, False, "generated_a1")

    pca.show_basic_data(True, False, "all")
    #
    # pca.show_clusters_dbscan(True, True, "dbscan", 1.4, 15)
    # pca.show_clusters_kmeans(True, False, "kmeans", 10)
    # pca.show_clusters_ap(True, False, "affprop", 0.7)
    # pca.show_clusters_meanshift(True, False, "meanshift")
    # pca.show_clusters_optics(True, False, "optics", 0.75, 10)

    # pca.vis()

    # tsne = TsneReduction(10, all_data)
    #
    # tsne.highlight_filtered_data("Custom function", True, True, "custom")
    # tsne.highlight_filtered_data("CEC2017", True, True, "cec2017")
    # tsne.highlight_filtered_data("CEC2022", True, True, "cec2022")
    # tsne.highlight_filtered_data("Generated", True, True, "generated")
    # tsne.show_basic_data(True, True, "all")
    #
    # tsne.show_clusters_dbscan(True, True, "dbscan", 1.4, 12)
    # tsne.show_clusters_kmeans(True, True, "kmeans", 7)
    # tsne.perplexity_scan()

    pass

if __name__ == "__main__":
    test()
