import pflacco.classical_ela_features as cf
from pflacco.sampling import create_initial_sample
import pandas as pd
import time
import print_time

features = {
    # calculate_dispersion,
    "disp.ratio_mean_02",
    "disp.diff_mean_02",
    # calculate_ela_meta
    "ela_meta.lin_simple.coef.min",
    "ela_meta.lin_simple.coef.max_by_min",
    "ela_meta.lin_w_interact.adj_r2",
    "ela_meta.quad_simple.adj_r2",
    "ela_meta.quad_simple.cond",
    "ela_meta.quad_w_interact.adj_r2",
    # calculate_ela_level
    "ela_level.mmce_lda_10",
    # "ela_level.mmce_mda_10",
    "ela_level.lda_qda_10",
    # "ela_level.lda_mda_10",
    "ela_level.mmce_lda_25",
    "ela_level.lda_qda_25",
    # "ela_level.qda_mda_25",
    "ela_level.mmce_lda_50",
    "ela_level.mmce_qda_50",
    "ela_level.lda_qda_50",
    # "ela_level.lda_mda_50",
    # "ela_level.qda_mda_50",
    # calculate_ela_distribution
    "ela_distr.skewness",
    "ela_distr.kurtosis",
    "ela_distr.number_of_peaks",
    # calculate_information_content
    "ic.h_max",
    "ic.m0",
    # calculate_nbc
    "nbc.nn_nb.sd_ratio",
    "nbc.nn_nb.cor",
    "nbc.dist_ratio.coeff_var",
    "nbc.nb_fitness.cor",
    # calculate_pca
    "pca.expl_var.cov_init",
    "pca.expl_var_PC1.cov_init",
    "pca.expl_var_PC1.cor_init"}

calculations = [
    cf.calculate_dispersion,
    cf.calculate_ela_meta,
    cf.calculate_ela_level,
    cf.calculate_ela_distribution,
    cf.calculate_information_content,
    cf.calculate_nbc,
    cf.calculate_pca]


def analyze(formula, fun_class, fun_name, repeats, dim=10, lower_bound=-100, upper_bound=100):
    # start_time = time.time()
    columns_inited = False
    result_dictionary = {"Class": fun_class, "Name": fun_name}
    data_table = pd.DataFrame()
    for iteration in range(repeats):
        X = create_initial_sample(dim, sample_type='lhs', lower_bound=lower_bound, upper_bound=upper_bound)
        for calculation in calculations:
            try:
                y = X.apply(lambda x: formula.evaluate(x), axis=1)
            except Exception as e:
                raise Exception(f"Výjimka analyze_to_vector: {e}")

            raw_result = calculation(X, y)
            result = {}
            for key in raw_result.keys():
                if key in features:
                    result[key] = raw_result[key]
            result_dictionary = {**result_dictionary, **result}

        if not columns_inited:
            data_table = pd.DataFrame(columns=list(result_dictionary.keys()))
            columns_inited = True

        data_table.loc[len(data_table.index)] = result_dictionary.values()

    # end_time = time.time()
    # print(f"time: {print_time.float_to_time(end_time - start_time)}")
    return data_table


def analyze_to_vector(formula, repeats, dim=10, lower_bound=-100, upper_bound=100):
    # start_time = time.time()
    result = []
    for iteration in range(repeats):
        X = create_initial_sample(dim, sample_type='lhs', lower_bound=lower_bound, upper_bound=upper_bound)
        for calculation in calculations:
            try:
                y = X.apply(lambda x: formula.evaluate(x), axis=1)
            except Exception as e:
                raise Exception(f"Výjimka analyze_to_vector: {e}")

            raw_result = calculation(X, y)
            for key in raw_result.keys():
                if key in features:
                    result.append(raw_result[key])

    # end_time = time.time()
    # print(f"time: {print_time.float_to_time(end_time - start_time)}")
    return result
