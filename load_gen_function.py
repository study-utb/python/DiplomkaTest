from storage import FormulaStorage
from TestFunctions.gen_sets import GenSets

fs = FormulaStorage()
func = fs.load_formula("a", 1)

print(func.evaluate([1, 1, 3]))

gs = GenSets("")
func = gs.get_formula(1)

print(func.evaluate([1, 1, 3]))
