import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import data_gatherer as gsd
import TestFunctions.bm_sets as bms


class DendrogramPlotter:
    def __init__(self, dim):
        # vytvoření potřebných adresářů, pokud neexistují
        if not os.path.exists(f"Images"):
            os.makedirs(f"Images")
        self.__dim = dim
        self.__filter_name = ""
        self.__diagram_title = ""
        self.__file_name = ""
        self.__filter = 0
        self.set_filter(self.__filter)
        data_gatherer = gsd.DataGatherer(dim)
        self.__normalized_data = data_gatherer.get_normalized_data_table()
        del data_gatherer

    def set_filter(self, filter_num):
        self.__filter = filter_num
        match filter_num:
            case 1:
                self.__filter_name = "Custom function"
                self.__diagram_title = "vlastních"
                self.__file_name = "custom"

            case 2:
                self.__filter_name = "CEC2017"
                self.__diagram_title = "CEC2017"
                self.__file_name = "cec2017"

            case 3:
                self.__filter_name = "CEC2022"
                self.__diagram_title = "CEC2022"
                self.__file_name = "cec2022"

            case _:
                self.__filter_name = ""
                self.__diagram_title = "všech"
                self.__file_name = "all"

    def plot_dendrogram(self, show, save):
        if self.__filter == 0:
            selected = self.__normalized_data
        else:
            selected = self.__normalized_data.loc[self.__normalized_data["Class"] == self.__filter_name]

        selected = selected.drop(columns=["Class", "Name"], axis=1)
        # names = gsd.table["Name"]

        plt.rcParams['figure.figsize'] = [15, 10]

        linkage_data = linkage(selected, method='ward', metric='euclidean')
        dendrogram(linkage_data)

        # plt.title(f"Dendogram normalizovaných vzáleností {self.__diagram_title} funkcí")
        plt.xlabel("Číslo funkce")
        plt.ylabel("Score podobnosti")

        if save:
            plt.savefig(f"Images/dendogram_{self.__file_name}_{self.__dim}D.png")
        if show:
            plt.show()
        plt.close()


def main():
    plotter = DendrogramPlotter(10)
    for i in range(4):
        plotter.set_filter(i)
        plotter.plot_dendrogram(False, True)

    del plotter


if __name__ == "__main__":
    main()
