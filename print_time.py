

def float_to_time(f_val, time_zone=0):
    f_val += time_zone * 60 * 60  # +2h
    val = int(f_val) % 86400
    ms = int((f_val % 1) * 1000)
    hh = val // 3600
    res = val % 3600
    mm = res // 60
    ss = res % 60
    return f"{str(hh).zfill(2)}:{str(mm).zfill(2)}:{str(ss).zfill(2)}:{str(ms).zfill(3)}"



