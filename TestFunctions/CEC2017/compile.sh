#!/bin/bash
echo There are two options for CEC2017 benchmarks. Standard, or fast_pow.
echo You can read something about fast_pow in README.txt:

echo
cat README.txt
echo
echo
echo Select what you want:

echo 1\) If you want standard CEC2017 press \"1\" and enter
echo 2\) If you want fast_pow CEC2017 press \"2\" and enter
echo 3\) If you want exit, just press enter

read selected

case $selected in
    1)
    echo You chose standard CEC2017
    gcc -fPIC -shared -lm -o cec2017lib.so cec17_test_func.c
    ;;
    2)
    echo You chose fast_pow CEC2017
    gcc -fPIC -shared -lm -o cec2017lib.so cec17_test_fast_pow.c
    ;;
    *)
    echo You chose the exit
    ;;
esac



