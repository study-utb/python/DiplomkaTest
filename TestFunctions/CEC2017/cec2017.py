from ctypes import CDLL, POINTER, c_int, c_double, c_char_p, c_char
import os


# x = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]    x: Solution vector
# nx = 10                               nx: Number of dimensions
# mx = 1                                mx: Number of objective functions
# func_num = 1                          func_num: Function number
# f = [0]                               Pointer for the calculated fitness

# def cec17_test_func(x, f, nx, mx, func_num):
def cec17_test_func(x, f, nx, mx, func_num):
    path = os.path.dirname(os.path.abspath(__file__))
    functions = CDLL(path + "/cec2017lib.so")
    x_pointer_type = POINTER(c_double * nx)
    f_pointer_type = POINTER(c_double * mx)
    nx_type = c_int
    mx_type = c_int
    func_num_type = c_int
    functions.cec17_test_func.argtypes = [x_pointer_type, f_pointer_type, nx_type, mx_type, func_num_type, POINTER(c_char)]
    functions.cec17_test_func.restype = None
    x_ctype = (c_double * nx)()
    for i, value in enumerate(x):
        x_ctype[i] = value
    f_ctype = (c_double * mx)()
    for i in range(mx):
        f_ctype[i] = 0

    functions.cec17_test_func(x_pointer_type(x_ctype), f_pointer_type(f_ctype), nx, mx, func_num, bytes(path, 'utf-8'))
    for i in range(len(f)):
        f[i] = f_ctype[i]


names = ["bent_cigar_func", "sum_diff_pow_func", "zakharov_func", "rosenbrock_func", "rastrigin_func",
         "schaffer_F7_func", "bi_rastrigin_func", "step_rastrigin_func", "levy_func", "schwefel_func",
         "hf01", "hf02", "hf03", "hf04", "hf05", "hf06", "hf07", "hf08", "hf09", "hf10",
         "cf01", "cf02", "cf03", "cf04", "cf05", "cf06", "cf07", "cf08", "cf09", "cf10"]

