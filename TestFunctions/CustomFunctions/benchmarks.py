import math
import numpy as np


def bukin_function_n6(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim - 1):
        x1 = val[i] * 0.05 - 10
        x2 = val[i+1] * 0.03
        res += 100 * math.sqrt(abs(x2-0.01*x1**2)) + 0.01 * abs(x1 + 10)
    return res


def ackley_ii(val):
    res = 0
    dim = val.shape[0]

    a1 = 0
    a2 = 0
    for i in range(0, dim):
        x1 = val[i] * 0.32768
        a1 += x1**2
        a2 += math.cos(2 * math.pi * x1)

    res = -20 * math.exp(-0.2 * math.sqrt(1/dim * a1)) - math.exp(1/dim * a2) + 20 + math.exp(1)
    return res


def cross_in_tray(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim - 1):
        # 0.02 pro rozsah -2..+2; 0.1 pro rozsah -10..+10
        x1 = val[i] * 0.1
        x2 = val[i+1] * 0.1
        res += -0.0001 * ((abs(math.sin(x1) * math.sin(x2) *
                               math.exp(abs(100 - (math.sqrt(x1**2 + x2**2) / math.pi)))) + 1)**0.1)
    return res


def drop_wave(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim - 1):
        # 0.02 pro rozsah -2..+2; 0.0512 pro rozsah -5.12..+5.12
        x1 = val[i] * 0.0512
        x2 = val[i+1] * 0.0512
        xx = x1**2 + x2**2
        res += -((1 + math.cos(12 * math.sqrt(xx))) / (0.5 * xx + 2))
    return res


def eggholder(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim - 1):
        # 5.12 pro rozsah -512..+512
        x1 = val[i] * 5.12
        x2 = val[i+1] * 5.12
        res += (-(x2 + 47) * math.sin(math.sqrt(abs(x2 + (x1 / 2) + 47)))
                - x1 * math.sin(math.sqrt(abs(x1 - (x2 + 47)))))
    return res


def gramacy_lee(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim):
        # * 0.01 + 1.5 pro rozsah +0.5..+2.5
        x1 = val[i] * 0.01 + 1.5
        res += (math.sin(10 * math.pi * x1)/(2*x1)) + (x1 - 1)**4
    return res


def griewank(val):
    res = 0
    dim = val.shape[0]
    a1 = 0
    a2 = 1
    for i in range(0, dim):
        # * 6 + pro rozsah -600..+600; 0.05 pro -5..+5; 0.1 pro -10..+10; 0.5 pro -50..+50
        x1 = val[i] * 6
        a1 += x1**2 / 4000
        a2 *= math.cos(x1 / math.sqrt(i+1))
    res = a1 - a2 + 1
    return res


def langermann(val):
    res = 0
    dim = val.shape[0]
    vector_c = np.array([1, 2, 5, 2, 3])
    matrix_a = np.array([[3, 5, 2, 1, 7],   # pro liché dimenze
                         [5, 2, 1, 4, 9]])  # pro sudé dimenze

    for j in range(0, 5):
        a1 = 0
        for i in range(0, dim):
            # * 0.05 + 5 pro rozsah 0..+10
            x1 = val[i] * 0.05 + 5
            a1 += (x1 - matrix_a[i % 2, j])**2
        res += vector_c[j] * math.exp(-a1 / math.pi) * math.cos(math.pi * a1)

    return res


def levy_omega(x):
    # * 0.1 pro rozsah -10..+10
    x = x * 0.1
    return 1 + ((x - 1) / 4)


def levy(val):
    dim = val.shape[0]
    # * 0.1 pro rozsah -10..+10
    om_1 = levy_omega(val[0])
    om_d = levy_omega(val[dim - 1])
    a1 = 0
    for i in range(0, dim-1):
        om_i = levy_omega(val[i])
        a1 += (om_i - 1)**2 * (1 + 10 * math.sin(math.pi * om_i + 1)**2)
    if dim-1 == 0:
        a1 += (om_1 - 1) ** 2 * (1 + 10 * math.sin(math.pi * om_1 + 1)**2)

    res = math.sin(math.pi * om_1)**2 + a1 + (om_d - 1)**2 * (1 + math.sin(2 * math.pi * om_d)**2)
    return res


def levy_n13(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim-1):
        # * 0.1 pro rozsah -10..+10
        x1 = val[i] * 0.1
        x2 = val[i+1] * 0.1
        res += (math.sin(3 * math.pi * x1)**2
                + (x1 - 1)**2 * (1 + math.sin(3 * math.pi * x2)**2)
                + (x2 - 1)**2 * (1 + math.sin(2 * math.pi * x2)**2))

    return res


def rastrigin(val):
    dim = val.shape[0]
    a1 = 0
    for i in range(0, dim):
        # * 0.0512 pro rozsah -5.12..+5.12
        x1 = val[i] * 0.0512
        a1 += (x1**2 - 10 * math.cos(2 * math.pi * x1))
    res = 10 * dim + a1
    return res


def schaffer_n2(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim-1):
        # rozsah -100..+100
        x1 = val[i]
        x2 = val[i+1]
        res += 0.5 + (math.sin(x1**2 - x2**2)**2 - 0.5) / ((1 + 0.001 * (x1**2 + x2**2))**2)

    return res

def schaffer_n4(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim-1):
        # * 0.5 pro rozsah -50..+50
        x1 = val[i] * 0.5
        x2 = val[i+1] * 0.5
        res += 0.5 + (math.cos(math.sin(abs(x1**2 - x2**2)))**2 - 0.5) / ((1 + 0.001 * (x1**2 + x2**2))**2)

    return res


def schwefel(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim):
        # * 5 pro rozsah -500..+500
        x1 = val[i] * 5
        res += x1 * math.sin(math.sqrt(abs(x1)))
    res = 418.9829 * dim - res
    return res


def shubert(val):
    res = 1
    dim = val.shape[0]
    for i in range(0, dim):
        # * 0.0512 pro rozsah -5.12..+5.12; *0.1 pro -10..+10
        x1 = val[i] * 0.1
        a1 = 0
        for j in range(1, 6):
            a1 += j * math.cos((j + 1) * x1 + j)
        res *= a1
    return res


def holder_table(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim-1):
        # * 0.1 pro rozsah -10..+10
        x1 = val[i] * 0.1
        x2 = val[i+1] * 0.1
        res += -abs(math.sin(x1) * math.cos(x2) * math.exp(abs(1 - (math.sqrt(x1**2 + x2**2)/math.pi))))

    return res

def sine(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim-1):
        # * 0.1 pro rozsah -10..+10; *0.15 pro -15..+15
        x1 = val[i] * 0.1
        x2 = val[i+1] * 0.1
        res -= 0.5 + ((math.sin(math.sqrt((x1**2) + (x2**2)) - 0.5)**2) / (1 + 0.001 * ((x1**2) + (x2**2)))**2)

    return res


def michalewicz(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim):
        # * 0.0157 + 1.57 pro rozsah 0..+3.14
        x1 = val[i] * 0.0157 + 1.57

        res -= math.sin(x1) * math.sin(((i+1) * x1**2) / math.pi)**20

    return res


def himmelblau(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim-1):
        # * 0.05 pro rozsah -5..+5
        x1 = val[i] * 0.05
        x2 = val[i+1] * 0.05
        res += (x1**2 + x2 - 11)**2 + (x1 + x2**2 - 7)**2

    return res


def styblinsky_tang(val):
    res = 0
    dim = val.shape[0]
    for i in range(0, dim):
        # * 0.05 pro rozsah -5..+5
        x1 = val[i] * 0.05
        res += x1**4 - 16 * x1**2 + 5 * x1
    res = res/2
    return res


bench = [bukin_function_n6, ackley_ii, cross_in_tray, drop_wave, eggholder,
            gramacy_lee, griewank, langermann, levy, levy_n13,
            rastrigin, schaffer_n2, schaffer_n4, schwefel, shubert,
            holder_table, sine, michalewicz, himmelblau, styblinsky_tang]


bench_names = ["Bukin Function N. 6", "Ackley Function II", "Cross-In-Tray Function", "Drop-Wave Function", "Eggholder Function",
            "Grammacy&Lee (2012) Function", "Griewank Function", "Langermann Function", "Levy Function", "Levy Function N. 13",
            "Rastrigin Function", "Schaffer Function N. 2", "Schaffer Function N. 4", "Schwefel Function", "Shubert Function",
            "Holder Table Function", "Sine Envelope Function", "Michalewicz's Function", "Himmelblau Function", "Styblinsky-Tang Function"]


