import numpy as np
import pandas as pd
import pflacco.sampling
import os
import importlib.util

import TestFunctions.CEC2017.cec2017 as cec2017
import TestFunctions.CEC2022.cec2022 as cec2022
import TestFunctions.CustomFunctions.benchmarks as ben

# x = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]    x: Solution vector
# nx = 10                               nx: Number of dimensions
# mx = 1                                mx: Number of objective functions
# func_num = 1                          func_num: Function number
# f = [0]                               Pointer for the calculated fitness


class BmSets:
    def __init__(self):
        self.functions_count = 0
        # počet předdefinovaných funkcí
        self.__predefined_func_count = 20
        self.functions_count += self.__predefined_func_count
        # počet funkcí cec 2017
        self.__cec2017_func_count = 19
        self.functions_count += self.__cec2017_func_count
        # počet funkcí cec 2022
        self.__cec2022_func_count = 11
        self.functions_count += self.__cec2022_func_count

    def get_fun_name(self, fce):
        if fce < 0 or fce > self.functions_count:
            raise Exception(f"fce musí být v rozsahu 0 - {self.functions_count - 1}, bylo zadáno: {fce}")

        if fce < self.__predefined_func_count:
            return ben.bench_names[fce]
        fce -= self.__predefined_func_count

        if fce < self.__cec2017_func_count:
            if fce >= 10:
                fce += 10
            return cec2017.names[fce]
        fce -= self.__cec2017_func_count

        if fce < self.__cec2022_func_count:
            return cec2022.names[fce]
        fce -= self.__cec2022_func_count

    def get_fun_class(self, fce):
        if fce < 0 or fce > self.functions_count:
            raise Exception(f"fce musí být v rozsahu 0 - {self.functions_count - 1}, bylo zadáno: {fce}")

        if fce < self.__predefined_func_count:
            return "Custom function"
        fce -= self.__predefined_func_count

        if fce < self.__cec2017_func_count:
            return "CEC2017"
        fce -= self.__cec2017_func_count

        if fce < self.__cec2022_func_count:
            return "CEC2022"
        fce -= self.__cec2022_func_count

    def count(self, fce, x):
        if type(x) is not pd.Series:
            raise Exception(f"x musí být Pandas Series, ale je {type(x)}")
        if fce < 0 or fce > self.functions_count:
            raise Exception(f"fce musí být v rozsahu 0 - {self.functions_count - 1}, bylo zadáno: {fce}")

        # pokud je číslo funkce v rozsahu předdefinovaných funkcí
        # předdefinovaná funkce mají hodnoty 0 - 19 (20 funkcí)
        if fce < self.__predefined_func_count:
            return self.__count_predefined(fce, x)
        fce -= self.__predefined_func_count

        # pokud je číslo funkce v rozsahu cec2017 funkcí
        # cec2017 funkce mají hodnoty 1 - 30 (29 funkcí)
        if fce < self.__cec2017_func_count:
            dim = x.shape[0]
            if fce >= 10:
                fce += 10
            return self.__count_cec2017(fce + 1, x, dim)
        fce -= self.__cec2017_func_count

        # pokud je číslo funkce v rozsahu cec2022 funkcí
        # cec2022 funkce mají hodnoty 1 - 12 (11 funkcí)
        if fce < self.__cec2022_func_count:
            return self.__count_cec2022(fce + 1, x)
        fce -= self.__cec2022_func_count

    def __count_predefined(self, fce, x):
        return ben.bench[fce](x)

    def __count_cec2017(self, fce, x, dim):
        f = [0]
        cec2017.cec17_test_func(x, f, dim, 1, fce)
        return f[0]

    def __count_cec2022(self, fce, x):
        new_x = x.values
        new_x = new_x.reshape(new_x.shape[0], 1)
        return cec2022.cec2022_func(func_num=fce).values(new_x).ObjFunc[0]


# x = pflacco.sampling.create_initial_sample(2, sample_type='lhs', lower_bound=-100, upper_bound=100)
# print(x)
# low_bound = -8
# hi_bound = -1
# shrink a
# a = (hi_bound - low_bound)/200
# shift b
# b = ((hi_bound - low_bound)/2) + low_bound
# z = x.apply(lambda bx: a * bx + b)
# print(z)

# fun = 50
# print(f"{get_fun_class(fun)}: {get_fun_name(fun)}")
# print(count(x, fun))

def main():
    sets = BmSets()
    print(sets.functions_count)
    for i in range(sets.functions_count):
        print(f"{sets.get_fun_class(i)} - {sets.get_fun_name(i)}")

    del sets


if __name__ == "__main__":
    main()

