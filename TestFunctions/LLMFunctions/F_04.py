import numpy as np

def evaluate(x):
    """
    A benchmark function with asymmetric landscape combining linear and nonlinear components.
    This function is designed to have a deceptive global structure with a mix of flat and steep regions.
    """
    # Nonlinear component with varying steepness
    nonlinear_component = np.sin(np.sum(x**2)) * np.exp(-np.sum(x**3))

    # Linear component to introduce asymmetry
    linear_component = 0.05 * np.sum(x)

    return nonlinear_component + linear_component

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-3, 3] are selected to encompass
    both the nonlinear and linear behaviors, providing a challenging and asymmetric landscape.
    """
    return [-3, 3]

