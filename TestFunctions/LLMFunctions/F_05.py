import numpy as np

def evaluate(x):
    """
    A benchmark function featuring a mix of discrete and continuous elements.
    This function is designed to have plateau-like structures interspersed with sharp transitions, 
    creating a landscape that challenges optimizers in terms of resolution and precision.
    """
    # Discrete-like component using floor function for plateau effect
    discrete_component = np.sum(np.floor(x**2))

    # Continuous component with sharp transitions
    continuous_component = np.sum(np.tanh(x))

    # Combining the components with a nonlinear twist
    return discrete_component + continuous_component**2

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-5, 5] are selected to encompass
    the interesting behaviors of both the discrete and continuous components of the function.
    """
    return [-5, 5]
