import numpy as np


def evaluate(x):

    if type(x) is not np.array:
        x = np.array(x)

    return np.sum(x**2) + np.sum(100 * (x[1:] - x[:-1]**2)**2)

def get_bounds():
    return [-5.0, 5.0]


