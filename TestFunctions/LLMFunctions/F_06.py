import numpy as np

def evaluate(x):
    """
    A benchmark function featuring random elements and conditional behavior.
    This function presents a dynamic landscape, challenging optimizers to adapt to unpredictability and varying conditions.
    """
    # Random component introducing non-determinism
    random_component = np.random.uniform(-1, 1) * np.sum(np.abs(x))

    # Conditional component based on the parity of the sum of squares
    sum_of_squares = np.sum(x**2)
    if sum_of_squares % 2 < 1:  # Checking if the floor of the sum is even
        conditional_component = np.sum(np.sin(x))
    else:
        conditional_component = np.sum(np.cos(x))

    return random_component + conditional_component

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-3, 3] are selected to allow 
    for sufficient variability in the function's behavior while ensuring numerical stability.
    """
    return [-3, 3]

