import numpy as np

def evaluate(x):
    """
    A benchmark function characterized by spatial heterogeneity and scaling behavior.
    This function presents an adaptive landscape where features change based on the scale of input values,
    challenging optimizers in terms of adaptability to different scales.
    """
    # Adaptive component changing with the magnitude of x
    adaptive_component = np.sum(np.where(np.abs(x) < 1, np.abs(x) * np.sin(1/x), np.log(1 + np.abs(x))))

    # Scaling component to introduce different behavior at various scales
    scaling_component = np.sum(np.where(np.abs(x) > 2, x**-2, np.sqrt(np.abs(x))))

    return adaptive_component + scaling_component

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-10, 10] are selected to cover
    a wide range of values, allowing the function to exhibit its adaptive and scaling behaviors.
    """
    return [-10, 10]
