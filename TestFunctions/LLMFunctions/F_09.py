import numpy as np

def evaluate(x):
    """
    A benchmark function with a dynamic and irregular landscape incorporating elements of randomness and non-uniformity.
    This function tests an optimizer's adaptability to unpredictable and irregular patterns.
    """
    # Randomly generated parameters for each evaluation to create a dynamic landscape
    np.random.seed(int(np.sum(np.abs(x) * 100)))  # Seed based on input to maintain consistency for the same x
    random_params = np.random.rand(*x.shape) * 10  # Random parameters in the range [0, 10]

    # Dynamic component introducing irregularity
    dynamic_component = np.sin(random_params * x) * np.exp(-0.1 * np.abs(x))

    # Non-uniform quadratic component for additional complexity
    non_uniform_quadratic = np.sum((x - random_params) ** 2)

    return np.sum(dynamic_component) + non_uniform_quadratic

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-5, 5] are selected to encompass
    the dynamic and non-uniform behaviors of the function, while keeping computational feasibility.
    """
    return [-5, 5]


