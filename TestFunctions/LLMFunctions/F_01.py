import numpy as np

def evaluate(x):
    """
    A new benchmark function that combines a sinusoidal component with a polynomial one.
    This function will have multiple local minima due to the sine function and the complexity increases with higher dimensions.
    """
    return np.sum(np.sin(x) * np.abs(x) ** 3 + 0.1 * x ** 2)

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds are wide enough to include
    interesting behavior of the function, especially given the oscillatory nature of the sine component.
    """
    return [-10, 10]
