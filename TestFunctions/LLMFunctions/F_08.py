import numpy as np

def evaluate(x):
    """
    A benchmark function emphasizing conditional landscape behavior and spatial discontinuities.
    This function creates a landscape with a mix of smooth and abrupt changes, challenging optimizers
    to handle sudden shifts and understand conditional dependencies.
    """
    # Conditional component introducing discontinuities
    conditional_component = np.sum(np.where(x < 0, np.exp(x) - 1, np.arctan(x)))

    # Piecewise component to add different behaviors in different regions
    piecewise_component = np.sum(np.where(x > 1, np.log(1 + np.abs(x)), 1 / (1 + np.exp(-x))))

    return conditional_component + piecewise_component

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-5, 5] are selected to allow
    the function to exhibit its unique conditional and piecewise behaviors effectively.
    """
    return [-5, 5]

