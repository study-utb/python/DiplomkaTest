import numpy as np

def evaluate(x):
    """
    A fractal-like benchmark function with self-similarity and high multimodality.
    This function uses a combination of a scaled Weierstrass function and a quadratic term,
    creating a challenging landscape with a complex global structure and many local optima.
    """
    # Fractal component - Weierstrass function-like behavior
    k_max = 10  # Determines the depth of the fractal behavior
    a, b = 0.5, 3  # Parameters controlling the fractal nature
    w_component = sum(a**k * np.cos(b**k * np.pi * x) for k in range(k_max))

    # Quadratic term to introduce a smoother global trend
    quadratic_component = 0.01 * np.sum(x ** 2)

    return np.sum(w_component + quadratic_component)

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-2, 2] are ideal to capture
    the intricate fractal behavior of the function while keeping the values within a manageable range.
    """
    return [-2, 2]
