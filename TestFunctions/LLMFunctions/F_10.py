import numpy as np

def evaluate(x):
    """
    A benchmark function emphasizing radial symmetry and layered complexity.
    This function creates a landscape with varying challenges in a radially symmetric manner, 
    offering a unique test for optimizers in radial navigation and depth exploration.
    """
    # Radial distance from the origin
    radial_distance = np.sqrt(np.sum(x**2))

    # Layered complexity - different functions at different radial distances
    if radial_distance < 3:
        inner_layer = np.sum(np.cos(x) * np.exp(-radial_distance))
    elif 3 <= radial_distance < 7:
        middle_layer = np.sum(np.tanh(x**2))
    else:
        outer_layer = np.sum(1 / (1 + radial_distance) + np.sin(x))

    return inner_layer if radial_distance < 3 else middle_layer if radial_distance < 7 else outer_layer

def get_bounds():
    """
    Returns the bounds for the function's domain. The chosen bounds [-10, 10] are selected to allow
    exploration of the different radial layers and their complexities.
    """
    return [-10, 10]
