import numpy as np
import pandas as pd
import os
from storage import FormulaStorage

class GenSets:
    def __init__(self, prefix: str):
        prefix = prefix.lower()
        self.__path = f"{os.path.dirname(os.path.abspath(__file__))}/Generated"
        self.formulas = []
        self.formula_names = []
        self.prefix = prefix
        self.__fs = FormulaStorage()

        self.__load_functions()

    def __load_functions(self):
        functions = []
        for file_name in os.listdir(self.__path):
            if file_name.startswith(f"f({self.prefix})") and file_name.endswith(".csv"):
                functions.append(int(file_name.split('-')[-1].split('.')[0]))
        functions.sort()

        for f in functions:
            form = self.__fs.load_formula(self.prefix, f)
            if form.dimension > 0:
                self.formulas.append(form)
                self.formula_names.append(f)

    def get_formula(self, name):
        if name not in self.formula_names:
            raise Exception("Funkce s tímto názvem se v sadě nenachází")
        return self.formulas[self.formula_names.index(name)]


def test():
    gs = GenSets("b")
    for f in gs.formulas:
        print(f.str())


if __name__ == "__main__":
    test()

