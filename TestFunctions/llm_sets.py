import numpy as np
import pandas as pd
import os
import importlib.util


class LlmSets:
    def __init__(self):
        self.functions_count = 0
        self.__imported_functions = []
        self.__imported_functions_bounds = []
        self.__imported_functions_names = []
        self.__load_imported_functions()

    def get_fun_name(self, fce):
        if fce < 0 or fce > self.functions_count:
            raise Exception(f"fce musí být v rozsahu 0 - {self.functions_count - 1}, bylo zadáno: {fce}")

        return self.__imported_functions_names[fce]

    def get_fun_class(self, fce):
        return "Imported function"

    def count(self, fce, x):
        if type(x) is not pd.Series:
            raise Exception(f"x musí být Pandas Series, ale je {type(x)}")
        if fce < 0 or fce > self.functions_count:
            raise Exception(f"fce musí být v rozsahu 0 - {self.functions_count - 1}, bylo zadáno: {fce}")

        return self.__count_imported(fce, x)

    def __count_imported(self, fce, x):
        low_bound = self.__imported_functions_bounds[fce][0]
        hi_bound = self.__imported_functions_bounds[fce][1]
        # každé x musí být upraveno do rozsahu low_bound..hi_bound pomocí rovnice y = a * x + b
        # shrink a
        a = (hi_bound - low_bound)/200
        # shift b
        b = ((hi_bound - low_bound)/2) + low_bound
        shrink_and_shift = lambda bx: a * bx + b
        bounded_x = shrink_and_shift(np.array(x))
        return self.__imported_functions[fce](bounded_x)

    def __load_imported_functions(self):
        imported_functions_path = f"{os.path.dirname(os.path.abspath(__file__))}/LLMFunctions"
        for file_name in os.listdir(imported_functions_path):
            if file_name == "F_template.py":
                continue
            if not file_name.endswith(".py"):
                continue
            file_path = os.path.join(imported_functions_path, file_name)
            spec = importlib.util.spec_from_file_location("module", file_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            get_bounds = self.__get_get_bounds(module, file_path)
            if get_bounds is None:
                continue
            evaluate = self.__get_evaluate(module, file_path)
            if evaluate is None:
                continue
            self.__imported_functions.append(evaluate)
            self.__imported_functions_bounds.append(get_bounds())
            self.__imported_functions_names.append(file_name.removesuffix(".py"))
            self.functions_count += 1

    def __get_get_bounds(self, module, path):
        if hasattr(module, "get_bounds") and callable(module.get_bounds):
            return module.get_bounds
        else:
            print(f"Nebyla nalezena funkce get_bounds v modulu {path}")
            return None

    def __get_evaluate(self, module, path):
        if hasattr(module, "evaluate") and callable(module.evaluate):
            return module.evaluate
        else:
            print(f"Nebyla nalezena funkce evaluate v modulu {path}")
            return None

def main():
    sets = LlmSets()
    print(sets.functions_count)
    for i in range(sets.functions_count):
        print(f"{sets.get_fun_class(i)} - {sets.get_fun_name(i)}")

    del sets


if __name__ == "__main__":
    main()

