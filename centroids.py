from data_gatherer import DataGatherer
from sklearn.cluster import KMeans
from data_gatherer import DataGatherer
import os
import numpy as np


def kmeans_clusters():
    data_gatherer = DataGatherer(10)
    original_data_scaled = data_gatherer.original_data_scaled
    kmeans = KMeans(n_clusters=6, n_init='auto')
    kmeans.fit(original_data_scaled)
    return kmeans.cluster_centers_

def ela_centroids():
    centroids = []
    data_gatherer = DataGatherer(10)
    rows_per_function = 30
    sub_lists = [data_gatherer.original_data_scaled[i:i+rows_per_function] for i in range(0, len(data_gatherer.original_data_scaled), rows_per_function)]
    for sl in sub_lists:
        centroids.append([sum(x)/len(sl) for x in zip(*sl)])

    return centroids

def save_clusters(vector, file_name):
    path = f"{os.path.dirname(os.path.abspath(__file__))}/Data"
    array = np.array(vector)
    np.savetxt(f"{path}/{file_name}.csv", array, delimiter=",")


def load_clusters(file_name):
    path = f"{os.path.dirname(os.path.abspath(__file__))}/Data"
    array = np.loadtxt(f"{path}/{file_name}.csv", delimiter=",")
    return array


def test():
    # k_clusters = kmeans_clusters()
    # print(k_clusters)
    # save_clusters(k_clusters, "kmeans_clusters")
    ela_c = ela_centroids()
    # print(ela_c)
    save_clusters(ela_c, "ela_centroids")


if __name__ == "__main__":
    test()
