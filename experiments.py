
#              key: prefix, soubor clusterů, velikost funkce, maxFEs
experiments = {"a": ["a", "kmeans_clusters", 20, 5000],
               "b": ["b", "ela_centroids", 20, 5000],
               "c": ["c", "kmeans_clusters", 30, 5000],
               "d": ["d", "ela_centroids", 30, 5000],
               "e": ["e", "kmeans_clusters", 30, 10000],
               }
