import math
from Strategy.DISH import DISH
from Strategy.CLPSO import CLPSO
from Strategy.Dbl_SHADE import Dbl_SHADE
from AnalyticalProgramming.Formula import Formula
from TestFunctions.gen_sets import GenSets
from TestFunctions.bm_sets import BmSets
from storage import ConvergenceStorage
import multiprocessing as mp


class TestBenchmark:
    def __init__(self):
        self.cs = ConvergenceStorage()

    def run_benchmark(self, prefix, count, dim, maxFEs, strategy):
        gs = GenSets(prefix)
        for i in range(len(gs.formula_names)):
            print(f"Benchmark {prefix} strategie {strategy} přechází na funkci {i}")
            form = gs.formulas[i]
            number = gs.formula_names[i]
            bests = []
            history = []

            match strategy:
                case 1:
                    if isinstance(form, Formula) and form.dimension > 0:
                        for j in range(count):
                            NP = round(25 * math.log(dim) * math.sqrt(dim))  # population size
                            H = 5  # archive size
                            minPopSize = 4

                            de = DISH(dim, (-100, 100), maxFEs, form, H, NP, minPopSize)
                            best, hist = de.run()
                            bests.append(best.features)
                            history.append(de.history_values)

                    else:
                        print(f"Formula {i} není platná funkce")

                    self.cs.save_convergence(bests, history, prefix, number, dim, "DISH")
                    bests.clear()
                    history.clear()
                case 2:
                    if isinstance(form, Formula) and form.dimension > 0:
                        for j in range(count):
                            # https://ieeexplore.ieee.org/document/1637688
                            # pro 10D problém populace 10, maxFEs 30k
                            # pro 30D problém populace 40, maxFEs 200k

                            NP = 20  # počet částic 10D
                            if dim == 30:
                                NP = 40  # počet částic 30D

                            max_iter = maxFEs / NP

                            pso = CLPSO(form.evaluate, dim, (-100, 100), NP, max_iter)
                            pos_best_g, err_best_g = pso.Run()

                            bests.append(pos_best_g)
                            history.append(pso.global_best_all_iteration)
                    else:
                        print(f"Formula {i} není platná funkce")

                    self.cs.save_convergence(bests, history, prefix, number, dim, "CLPSO")
                    bests.clear()
                    history.clear()
                case 3:
                    isok = True
                    if isinstance(form, Formula) and form.dimension > 0:
                        for j in range(count):
                            NP = 18 * dim  # population size
                            H = 10  # archive size
                            minPopSize = 4

                            de = Dbl_SHADE(dim, (-100, 100), maxFEs, form, H, NP, minPopSize)
                            try:
                                best = de.run()
                            except:
                                print(f"Vynechávám funkci {prefix}{number}")
                                isok = False
                                break

                            bests.append(best.features)
                            history.append(de.history_values)

                    else:
                        print(f"Formula {i} není platná funkce")

                    if isok:
                        self.cs.save_convergence(bests, history, prefix, number, dim, "DblSHADE")
                    bests.clear()
                    history.clear()
                case _:
                    print("Nedefinovaná strategie")
                    return

            # print(f"Benchmark {prefix} funkce {i} zahajuje DE/RAND/1/BIN")
            # if isinstance(form, Formula) and form.dimension > 0:
            #     for j in range(count):
            #         # https://ieeexplore.ieee.org/document/1637688
            #         # pro 10D problém populace 10, maxFEs 30k
            #         # pro 30D problém populace 40, maxFEs 200k
            #         dim = 10  # dimension size
            #         NP = 20  # populace
            #
            #         f = 0.6  # mutační constanta
            #         CR = 0.8  # crossover rate
            #
            #         de = DeRand1Bin(form.evaluate, dim, (-100, 100), NP, maxFEs, f, CR)
            #         de.run()
            #         bests.append(de.best.features)
            #         history.append(de.progress)
            # else:
            #     print(f"Formula {i} není platná funkce")
            #
            # print("DE/RAND/1/Bin:")
            # print(history[0][len(history[0]) - 1])
            # self.cs.save_convergence(bests, history, prefix, number, "DE")
            # bests.clear()
            # history.clear()


def worker(benchmark: TestBenchmark, prefix, count, dim, maxFEs, strategy):
    benchmark.run_benchmark(prefix, count, dim, maxFEs, strategy)


def test():
    # Nastavení pro 10D
    args_list = [("a", 30, 10, 30000, 1), ("a", 30, 10, 30000, 2), ("a", 30, 10, 30000, 3),
                 ("b", 30, 10, 30000, 1), ("b", 30, 10, 30000, 2), ("b", 30, 10, 30000, 3),
                 ("c", 30, 10, 30000, 1), ("c", 30, 10, 30000, 2), ("c", 30, 10, 30000, 3),
                 ("d", 30, 10, 30000, 1), ("d", 30, 10, 30000, 2), ("d", 30, 10, 30000, 3),
                 ("e", 30, 10, 30000, 1), ("e", 30, 10, 30000, 2), ("e", 30, 10, 30000, 3)]
    # Nastavení pro 30D
    # args_list = [("a", 30, 30, 200000, 1), ("a", 30, 30, 200000, 2), ("a", 30, 30, 200000, 3),
    #              ("b", 30, 30, 200000, 1), ("b", 30, 30, 200000, 2), ("b", 30, 30, 200000, 3),
    #              ("c", 30, 30, 200000, 1), ("c", 30, 30, 200000, 2), ("c", 30, 30, 200000, 3),
    #              ("d", 30, 30, 200000, 1), ("d", 30, 30, 200000, 2), ("d", 30, 30, 200000, 3),
    #              ("e", 30, 30, 200000, 1), ("e", 30, 30, 200000, 2), ("e", 30, 30, 200000, 3)]

    processes = []

    for args in args_list:
        benchmark = TestBenchmark()
        process = mp.Process(target=worker, args=(benchmark,) + args)
        processes.append(process)
        process.start()

    for process in processes:
        process.join()

    # maxFEs = 30000  # maximum number of objective function evaluations (10D)
    # ben = TestBenchmark()
    # ben.run_benchmark('a', 30, 10, maxFEs)

    # maxFEs = 200000  # maximum number of objective function evaluations (30D)
    # ben.run_benchmark('a', 30, 30, maxFEs)
    # ben.run_benchmark('b', 30, 30, maxFEs)
    # ben.run_benchmark('c', 30, 30, maxFEs)
    # ben.run_benchmark('d', 30, 30, maxFEs)


if __name__ == '__main__':
    print("Benchmarky již byly dokončeny")
    # test()
