import os
import AnalyticalProgramming.AP as AP
from storage import FormulaStorage
from storage import HistoryStorage
from storage import ConvergenceStorage
import time
import print_time
import warnings
from loger import log
from experiments import experiments

warnings.filterwarnings("ignore", category=RuntimeWarning)

# definice experimentů je v souboru experiments.py
experiment = "e"
attempts = 20
fs = FormulaStorage()
hs = HistoryStorage()
cs = ConvergenceStorage()

for i in range(attempts):
    exp = experiments[experiment]
    log(f"Zahajuji pokus {i+1}/{attempts}, metoda: {exp[1]}, prefix: {exp[0]}", True)

    start_time = time.time()

    # "kmeans_clusters", "ela_centroids"
    ap = AP.AnalyticalProgramming(exp)

    best, history = ap.start()
    end_time = time.time()
    print()
    log(f"Dokončen pokus {i+1}", True)
    log("+--------------------------+", True)
    log(f"| total time: {print_time.float_to_time(end_time - start_time)} |", True)
    log("+--------------------------+", True)
    file = fs.save_formula(best, exp[0])
    hs.save_history(history, file)
    cs.save_convergence_ap(ap.strategy.history_values, exp[0])
    log(f"Uložen jako: {file}", True)
    print()
