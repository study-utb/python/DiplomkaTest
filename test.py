from TestFunctions.gen_sets import GenSets
from storage import ConvergenceStorage
import numpy as np
import matplotlib.pyplot as plt
from storage import FormulaStorage
from data_gatherer import GenDataGatherer

def test(prefix):
    # fs = FormulaStorage()
    # for i in range(1, 15):
    #     formula = fs.load_formula(prefix, i)
    #     print(f"{i}\n{formula.str()}\n******")
    # print(formula.dimension)

    gs = GenSets(prefix)
    for i in range(len(gs.formula_names)):
        print(gs.formula_names[i])
        # print(gs.formulas[i].str())
        print(gs.formulas[i].formula())
        print(gs.formulas[i].latex())

def get_ela_features():
    gdg = GenDataGatherer("z", False)

    fun = 9
    for i in range(3):
        data = gdg.data_table.drop(["Class", "Name"], axis=1)
        data = data.iloc[(fun-1)*30:(fun-1)*30 + 30, i*9:i*9 + 9]
        data.reset_index(inplace=True, drop=True)

        means = data.iloc[:, :].mean()
        stds = data.iloc[:, :].std()

        data.loc[len(data.index)] = means
        data.loc[len(data.index)] = stds
        data = data.iloc[30:, :]
        print(data.to_latex(index=False, float_format=lambda x: "{:.2E}".format(x)))



if __name__ == "__main__":
    # test("z")
    get_ela_features()
