import os
import AnalyticalProgramming.DiscreteSetHandling as Dsh
import numpy as np
from Strategy.AP_DISH import Individual
import csv


class FormulaStorage:
    def __init__(self):
        self.path = f"{os.path.dirname(os.path.abspath(__file__))}/TestFunctions/Generated"
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def __find_highest_numbered_file(self, prefix):
        existing_files = [file for file in os.listdir(self.path) if file.startswith(prefix)]
        if not existing_files:
            return 0
        else:
            highest_number = max(int(file.split('-')[-1].split('.')[0]) for file in existing_files)
            return highest_number

    def __generate_new_filename(self, prefix):
        highest_number = self.__find_highest_numbered_file(f"f({prefix})")
        next_number = highest_number + 1
        return f"f({prefix})-{next_number:06d}.csv"

    def save_formula(self, indi: Individual, prefix):
        vector = indi.features
        if indi.use_reinforced:
            vector = [vector, indi.reinforced]

        array = np.array(vector)
        filename = self.__generate_new_filename(prefix)
        np.savetxt(os.path.join(self.path, filename), array, delimiter=",")
        return filename

    def load_formula(self, prefix: str, number):
        prefix = prefix.lower()
        array = np.loadtxt(f"{self.path}/f({prefix})-{number:06d}.csv", delimiter=",")
        if array.shape[0] != 2:
            array = [array, []]

        formula = Dsh.create_formula(array)
        return formula



class HistoryStorage:
    def __init__(self):
        self.path = f"{os.path.dirname(os.path.abspath(__file__))}/Data/History"
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def save_history(self, indi: list[Individual], filename):
        history = []
        for ind in indi:
            history.append(ind.ofv)

        array = np.array(history)

        np.savetxt(os.path.join(self.path, filename), array, delimiter=",")


class ConvergenceStorage:
    def __init__(self):
        self.path = f"{os.path.dirname(os.path.abspath(__file__))}/Data/Convergence"
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def save_convergence(self, bests: list, history: list, prefix, number, dim, algo):
        filename = f"{self.path}/{algo}: f({prefix})-{number:06d}"
        b = np.array(bests)
        np.savetxt(f"{filename}-best-vectors-{dim}D.csv", b, delimiter=",")
        h = np.array(history)
        np.savetxt(f"{filename}-convergence-{dim}D.csv", h, delimiter=",")

    def save_convergence_ap(self, history: list, prefix):
        filename = f"{self.path}/AP: f({prefix})-convergence.csv"

        with open(filename, mode='a') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writerow(history)

    def load_convergence(self, prefix, number, dim, algo):
        filename = f"{self.path}/{algo}: f({prefix})-{number:06d}-convergence-{dim}D.csv"
        array = np.loadtxt(filename, delimiter=",")
        return array

    def load_bests(self, prefix, number, dim, algo):
        filename = f"{self.path}/{algo}: f({prefix})-{number:06d}-best-vectors-{dim}D.csv"
        array = np.loadtxt(filename, delimiter=",")
        return array

    def load_convergence_ap(self, prefix, filter: list):
        filename = f"{self.path}/AP: f({prefix})-convergence.csv"
        history = []
        index = 0
        with open(filename, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
            for row in csv_reader:
                if index not in filter:
                    history.append(row)
                index += 1
        return history


def test():
    # vector = [[16.70613603, 7.52277652, 13.03689871, 17.9631009, 3.14686621, 11.38976325, 7.15645009, 13.3797846, 6.33214402, 9.71939438, 16.55874517, 8.2786473, 13.62919227, 2.5896273, 5.07563819, 10.54361314, 14.91992208, 4.34738232, 10.61060962, 7.92190127, 0.35270892, 0.77862449, 0.30548302, 0.70699522, 0.38198621, 0.26741611, 0.81777339, 0.41089256, 0.65256108, 0.37298175, 0.49099871],
    #           [15.50106712, 3.34705629, 13.21353725, 12.27791493, 2.45847937, 14.22411732, 16.34151486, 14.53990766, 3.23628941, 11.93186814, 17.20285879, 8.31738216, 17.78486678, 7.26520674, 5.17775159, 1.22237162, 11.44605186, 1.89006266, 13.11757323, 9.10606839, 0.35980225, 0.82940643, 0.18288785, 0.45122439, 0.09683591, 0.36505758, 0.6911952, 0.53258102, 0.72492233, 0.46607877, 0.69183298]]
    #
    # indi = Individual(20, [0, 18], 11, [0, 1], 0)
    # indi.features = vector[0]
    # indi.reinforced = vector[1]
    # indi.use_reinforced = True
    #
    # formula = Dsh.create_formula(vector)
    # # print(formula.str())
    # fs = FormulaStorage()
    # print(fs.save_formula(indi, "test"))

    # formula = fs.load_formula("a", 1)


    # print(formula.str())

    cs = ConvergenceStorage()
    # "CLPSO", "DblSHADE", "DISH"
    arr = cs.load_convergence("a", 1, 10, "DISH")
    print(arr.shape)



    pass


if __name__ == "__main__":
    test()
